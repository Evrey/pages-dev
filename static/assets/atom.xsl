<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
  version="3.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:atom="http://www.w3.org/2005/Atom">
  <xsl:output
    method="html"
    version="5.0"
    encoding="UTF-8"
    indent="yes"
  	media-type="text/html"
  />
  <xsl:template match="/">

<html>
  <head>
    <meta charset="UTF-8" />
    <meta
      http-equiv="content-security-policy"
      content="default-src 'self'; script-src 'none';  media-src 'self'; img-src 'self'; connect-src 'none'; style-src 'self'"
    />
    <meta http-equiv="Referrer-Policy"    content="no-referrer" />
    <meta http-equiv="Permissions-Policy" content="interest-cohort=()" />
    <meta name="referrer"     content="no-referrer" />
    <meta name="theme-color"  content="#24C" />
    <meta name="color-scheme" content="normal" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, user-scalable=yes"
    />

    <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
    <title><xsl:value-of select="/atom:feed/atom:title"/></title>

    <link rel="stylesheet" href="/assets/css/fonts.css" />
    <link rel="stylesheet" href="/assets/css/main.css" />

    <link
      rel="author"
      hreflang="en"
      referrerpolicy="no-referrer"
      type="text/html"
    >
      <xsl:attribute name="href">
        <xsl:value-of select="/atom:feed/atom:author/atom:uri" />
      </xsl:attribute>
    </link>

    <meta name="canonical">
      <xsl:attribute name="content">
        <xsl:value-of select="/atom:feed/atom:link[@rel='self']/@href" />
      </xsl:attribute>
    </meta>
    <meta property="og:site_name">
      <xsl:attribute name="content">
        <xsl:value-of select="/atom:feed/atom:title" />
      </xsl:attribute>
    </meta>
    <meta property="og:locale">
      <xsl:attribute name="content">
        <xsl:value-of select="/atom:feed/@xml:lang" />
      </xsl:attribute>
    </meta>
    <meta name="og:url">
      <xsl:attribute name="content">
        <xsl:value-of select="/atom:feed/atom:link[@rel='self']/@href" />
      </xsl:attribute>
    </meta>
    <meta name="og:image"      content="/assets/img/rss-opt.svg" />
    <meta name="og:image:type" content="image/svg+xml" />
    <meta name="og:type"       content="feed" />
    <meta name="author">
      <xsl:attribute name="content">
        <xsl:value-of select="/atom:feed/atom:author/atom:name" />
      </xsl:attribute>
    </meta>
    <meta name="description">
      <xsl:attribute name="content">
        <xsl:value-of select="/atom:feed/atom:subtitle" />
      </xsl:attribute>
    </meta>
  </head>

  <body>
    <section class="box">
      <header>
        <hgroup class="site-title">
          <h1>
            <img
              alt=""
              class="icon"
              src="/assets/img/rss-opt.svg"
              width="1"
              height="1"
              decoding="async"
              loading="lazy"
            />
            <xsl:value-of select="/atom:feed/atom:title" />
          </h1>
          <p><xsl:value-of select="/atom:feed/atom:subtitle" /></p>
          <p>
            <small>
              <xsl:value-of select="/atom:feed/atom:rights" />
            </small>
          </p>
          <p>
            <small>
              <data>
                <xsl:attribute name="value">
                  <xsl:value-of select="/atom:feed/atom:id" />
                </xsl:attribute>
                <xsl:value-of select="/atom:feed/atom:id" />
              </data>
            </small>
          </p>
        </hgroup>

        <time class="box">
          <strong>Last Update:</strong> 
          <xsl:attribute name="datetime">
            <xsl:value-of select="/atom:feed/atom:updated" />
          </xsl:attribute>
          <xsl:value-of select="/atom:feed/atom:updated/@nice" />
        </time>
        <data class="box">
          <strong>Feed Entries:</strong> 
          <xsl:attribute name="value">
            <xsl:value-of select="count(/atom:feed/atom:entry)"/>
          </xsl:attribute>
          <xsl:value-of select="count(/atom:feed/atom:entry)"/>
        </data>
	    </header>

      <hr />

      <p>
        This is the <a
          href="https://en.wikipedia.org/wiki/Atom_(web_standard)"
          target="_blank"
          rel="noopener noreferrer nofollow external"
        >Atom feed</a> for <a class="trusted-ext">
          <xsl:attribute name="href">
            <xsl:value-of select="/atom:feed/atom:link[not(@rel)]/@href" />
          </xsl:attribute>
          <xsl:value-of select="/atom:feed/atom:title" />
        </a>. Put this feed’s URL into your favourite feed
        aggregator to keep track of updates to this site. In
        case you don’t have a feed aggregator yet, just install
        one. Some browsers have built-in feed aggregators, some
        have feed aggregator extensions, but there are also
        desktop and mobile apps you could use.
      </p>

      <ol reversed="true" type="1">
        <xsl:for-each select="/atom:feed/atom:entry">
          <li>
            <details>
              <summary>
                <a class="trusted-ext">
                  <xsl:attribute name="href">
                    <xsl:value-of select="atom:link[not(@rel)]" />
                  </xsl:attribute>
                  <xsl:value-of select="atom:title" />
                </a> | <time>
                  <xsl:attribute name="datetime">
                    <xsl:value-of select="atom:updated" />
                  </xsl:attribute>
                  <xsl:value-of select="atom:updated/@nice" />
                </time> | <small>
                  <data>
                    <xsl:attribute name="value">
                      <xsl:value-of select="atom:id" />
                    </xsl:attribute>
                    <xsl:value-of select="atom:id" />
                  </data>
                </small>
              </summary>

              <xsl:value-of
                select="atom:summary"
                disable-output-escaping="yes"
              />
            </details>
          </li>
        </xsl:for-each>
      </ol>
    </section>
  </body>
</html>

  </xsl:template>
</xsl:stylesheet>
