# pages-dev

Zola dev repo of my Codeberg Pages site.

## Images

Widths:

- `1920px`
- `1280px`
- `640px` @ 256 colours
- `320px` @ 256 colours

Format: AVIF

## Research List

- <https://caniuse.com/css-nesting>

