#!/usr/bin/env fish
set BlogUuidSeed 'da1864ea-fdf9-40ac-a792-9e7086787e22'
set BlogUuid     (uuidgen -N 'Evyl.blue' --sha1 -n $BlogUuidSeed)

uuidgen -N $argv[1] --sha1 -n "$BlogUuid"
