+++
title       = "Just Wayland, a Socket, and I"
description = "While working on a game engine project, i eventually decided to ditch the platform glue libraries i’ve been using so far. Instead, in good ol’ DooM or Handmade Hero fashion, i’m now working on talking to the underlying platforms directly. I even went so far as to ditch the wayland-client C library everyone uses. Was it worth the hassle? I think yes!"
draft       = true

[taxonomies]
categories = ["platform-glue"]
keywords   = ["platform-glue", "wayland", "linux", "data-format", "rust"]

[extra]
uuid = "d7df384a-cf16-5812-9036-095df74c2c30"
code = true
+++

Before we begin, i want to clarify that henceforth all code examples
or data type analogies are expressed in [Rust][rs], for that’s the
programming language i’m writing my game in. The working code snippets
are all `no_std` and only use `libc` and `libcore` as dependencies.

This is a long blog post. It won’t get you all the way to having a
window open and handle input events. But it’ll get you all the way to
knowing how to generally send requests, receive and handle events, and
manage object lifetimes. The rest you can gather from following the
excellent [Wayland Book][wl-book-0], despite it using the wayland-client
library.

## Our friend Winit

Those familiar with Rust are likely already familiar with the
[Winit][rs-winit] library. For the others: It pretty much is to Rust
what [GLFW][c-glfw] or [SDL2][c-sdl2] are for C and C++:
Get an application window — title and all — up and running in but a few
function calls, ready to pump some user input and other events, on as many platforms as possible.

I’m quite fond of the [Handmade Hero][hhero] approach to writing software:
Start from almost nothing and build up only exactly what you need. Winit
is a massive dependency, for many reasons, but mostly because it has to
cater to the needs of all kinds of software projects. I’m writing a game
engine for a specific game, however. I know exactly what features i need
from my windowing code. And so, i should be able to write highly specialised
code that is smaller, faster, and uses fewer resources than »the competition«.

## Starting with Wayland

My development machine is a KDE-based Linux distro, so while i do intend
to target more platforms† in the long run, a Linux backend is where i start.
Specifically: A Wayland backend, for since KDE6 i can safely start to assume
that my end users won’t run an X11 set-up.

As some of you might already know, [Wayland][wl] is a binary messaging
protocol between a compositor server and a client application. That protocol
is extensible with custom protocols‡ that add new server functionality a client
can utilise, beyond the scope of the core protocol. [Here][wl-app]’s a list
of well-known protocols and their documentation.

<aside>
  <p>
    † Windows and Android are the two other platforms i’ll definitely support.
    In the case of Windows, i am already familiar with how to set up a game
    window and input events with WinAPI, xinput, etc. Android support will be
    yet another learning jorney for me, however. I’m looking forward to it.
    And what other platforms i’ll end up supporting, only time may tell. Who
    knows, maybe there’ll be an UEFI backend.
  </p>
  <p>
    ‡ If Wayland protocol extensions sound a bit like custom UEFI interfaces
    to you, well, they are pretty much the same concept.
  </p>
</aside>

### libwayland-client.so

To get things started, i prototyped my platform code using just
`libwayland-client.so`, the official implementation of the Wayland
protocol. There’s an accompanying `libwayland-server.so` you can use
to write your own Wayland compositor, and to my knowledge, compositors
like KDE Plasma do use that library instead of building their own.

In my Rust code, i linked against the Wayland library directly. My
intention was always to drop helper libraries all together and just use
them to prototype, so with that in mind i wrote my own bindings.
<small>(It’s not Vulkan, where you encounter `struct`s with over 2000
properties.)</small>
My glue code looked pretty much like this:

```rs
// An empty marker type for opaque C types.
// !Sync, !Send, !Unpin, ZST
#[repr(C)]
struct Opaque(c_void, PhantomData<(*mut c_void, PhantomPinned)>);

#[repr(transparent)] struct wl_display (Opaque);
#[repr(transparent)] struct wl_callback(Opaque);

// Some functions are implemented in `libwayland-client.so`
#[link(name = "wayland-client", kind = "dylib")]
extern "C" {
  fn wl_display_flush(*mut wl_display) -> u32;
}

// Others are generated with wayland-scanner.
unsafe extern "C"
fn wl_display_sync(display: *mut wl_display) -> *mut wl_callback {
  …
}
```
No surprises there. Now you may wonder:
<q>Why not stay with wayland-client if it works well?</q>

Consider `wl_display_sync`. What it does is send a »sync request« to
the compositor, which will eventually respond with a corresponding
»callback event«.

#### What’s happening inside?

<details>
  <summary>Spoiler</summary>
  <p>
    This paragraph is intentionally confusing and exhausting to read.
    Though it is truthful and in fact far from complete. Feel free to
    skip ahead to »Is it necessary?« with the summary:
    »A lot happens inside!«
  </p>
</details>

When we call `wl_display_sync`, it in turn calls `wl_proxy_marshal_flags`,
which calls `wl_argument_from_va_list` to unpack our `va_list` parameters
into a dynamically typed array and then forwards that array of dynamically
typed arguments to `wl_proxy_marshal_array_flags`. *That* function will
then first build the raw `&[u32]` of our request by calling
`wl_closure_marshal`, which involves calling `malloc`, to then call
`wl_closure_send`, and then finish the request by cleaning up our
request `&[u32]` with `wl_closure_destroy`. `wl_closure_send` then
`malloc`s yet another buffer into which that `&[u32]` is actually written,
then sends the request off with `wl_connection_write`, which in turn
calls `wl_connection_queue`, which appends the request to wayland-client’s
I/O ring buffer, and eventually a call to `wl_display_flush` calls
`wl_connection_flush`, which accumulates all requests enqueued in the ring
buffer to send them off all at once with the `sendmsg` syscall.

#### Is it necessary?

Sounds complicated? Well, it is. Here’s the bare minimum of what you need
to successfully perform the equivalent `wl_display_sync` operation:
```rs
let next_free_id = /* we’ll get to that, but it’s easy */;
let sync_request: [u32; 3] = [1, 0x000C_0000, next_free_id];

//  ↓ We need to check `errno` if this is −1, but i also left
//  ↓ out all the error handling inside libwayland-client.so
let result = unsafe { send(
  wl_socket_fd,
  sync_request.as_ptr() as *const c_void,
  sync_request.len() * size_of::<u32>(),
  MSG_NOSIGNAL,
)};
// Notice how not a single heap allocation needed to be performed.
```
All it takes is sending a sequence of 3 magic words and you performed
a sync request. So what is wayland-client doing? <q>Is it bloat?</q>
you might now think.

Well, let’s not get hasty. Unlike this short Rust code above, the
wayland-client library »doesn’t know« what a »sync request« is. It only
knows how to perform dynamic typing and with dynamic typing and meta
programming build arbitrary messages of arbitrary protocols. This allows
you to use all the hottest and newest protocols your client supports,
without having to wait for updates in `libwayland-client.so`. Further
more, the library manages internal ring buffers and mutexes because it
doesn’t know the patterns in which your application will send requests.

I, on the other hand, do know what exact protocols i’ll be using and
when and how i will aggregate and send off requests, even when i’ll
be polling events coming back from the server. Thus, i don’t need the
dynamically-typed meta programming magic, allowing me to get rid of
most of wayland-client’s code. It’s really just a stateful messaging
codec.

### The wire format

Now that you’ve seen 12 entire bytes of a real Wayland message, let’s
have a look at a message’s anatomy.

Being a binary messaging protocol, we need to know how to encode and
decode messages between the client and server. The Wayland protocol
[wire format][wl-fmt] is optimised for IPC, i.e. messaging on the local
machine, not for RPC, i.e. messaging across networked systems. As such,
Wayland favours fast message encoding and decoding over compact
representations.

All Wayland messages consist of chunks that are 4 bytes in size. In
Rust terms that means every message can be thought of as a `&[u32]`.
The Wayland protocol makes no mention <small>(unless i missed it)</small>
of what to do if you, say, receive 3 bytes and only much later the missing
last byte arrives. I assume the Wayland server will, just like i’ll be
doing, send all messages as `&[u32]`, so splits like that shouldn’t
happen. As such, i am treating such cases as an indicator of client and
server having fallen out of sync, meaning i should drop the current
connection and set up a new one. So far this »out of sync« condition
hasn’t occurred for me.

#### The new_id pitfall

I won’t go into too much detail for how messages are encoded, for the
linked [wire-format][wl-fmt] page alreads does so briefly. Instead, i
highlight a specific section of that chapter that caused me a few head
aches:

<figure>
  <blockquote>
    <p><strong>new_id</strong></p>
    <p>
      The 32-bit object ID. Generally, the interface used for the new object
      is inferred from the xml, but in the case where it's not specified,
      a new_id is preceded by a string specifying the interface name, and
      a uint specifying the version.
    </p>
  </blockquote>
</figure>

Let’s ignore that XML mention for a moment. This is the one piece of
information that is easy to forget, because it is so surprising in its
design. Let me paraphrase: When you read `new_id` in a protocol schema,
it *sometimes* means it’s a `u32` ID. Other times it’s actually a
`(&str, u32, u32)`. Triple-check your code when you generate messages
that take a `new_id` parameter. [Here][wl-oops]’s a mailing list post
of another person who fell for that. They have my thanks, for that
mail and reply reminded me of that pitfall.

#### The XML schema

As the above quote mentions, Wayland protocols are defined in XML-based
schemas. The idea is to ship just one schema XML per protocol and use
automated tools like `wayland-scanner` to generate programming-language-specific
code to be used with wayland-client or wayland-server. Now, while i don’t
generate or use these bindings, i still have to make sense of the XML
schema to correctly generate messages in Wayland’s wire format.

The already mentioned [Wayland.app][wl-app] website provides a nicely
rendered user interface representation of these XML schemas.
[This here][wl-sync], for example, is the documentation of the
`wl_display::sync` request, pulled verbatim from the original
`wayland.xml` schema file of the core protocol. Annoyingly, one important
piece of information does *not* appear anywhere in that website’s UI.
Here’s another quote from the [wire format specification][wl-fmt]:

<figure>
  <blockquote>
    <p>
      The second [word of a message] has 2 parts of 16-bit. […]
      <em>The lower is the request/event opcode.</em>
    </p>
  </blockquote>
</figure>

Looking at the online schema documentation, the opcode of
`wl_display::sync` is nowhere to be seen. Probably because it also
doesn’t appear anywhere in `wayland.xml`. So how do code generators
know what the corresponding opcodes are?

The answer is quite simple: In your mind, you have to keep two counters,
starting at 0. One counter for »requests«, the other counter for »events«.
Each pair of counters is unique to a specific interface, i.e. every
interface gets its own counter pair starting at 0. Now you read all
requests and events of an interface in order. Their opcodes are the
current counter values, and whenever you go to the next request or
event, you increment the corresponding counter by 1.

So for `wl_display`, the opcode for `sync` is `0`, the opcode for
`get_registry` is `1`, and the opcode for `error` is `0`. I still wish
that website just added the opcodes to the UI.

With that knowledge, we can make sense of our `sync` request way above:
```rs
// object ID 1 = wl_display
// ↓
// ↓       12 message bytes in total
// ↓       ↓
// ↓       ↓ opcode 0 = sync
// ↓       ↓ ↓
// ↓       ↓ ↓     new_id for a wl_callback
// ↓       ↓ ↓     ↓
  [1, 0x000C_0000, next_free_id]
```

## Where’s my server?

Before we can start exchanging messages with the Wayland server, we need
an open communication channel. It’s just a good ol’ UNIX domain socket.
Finding that socket involves a wee bit of work, however.

1. By default, that socket is called `wayland-0`.
2. However, if the `WAYLAND_DISPLAY` environment variable is set, one
   of these rules applies:
   1. If that text value starts with a `/`, take it as an arbitrary
      absolute filesystem path to the Wayland socket.
   2. Otherwise, this is the name or a *relative* path to that socket.
3. Wait, relative to where? It’s not mentioned anywhere, but it’s relative
   to the path stored in `XDG_RUNTIME_DIR`.
   1. If `XDG_RUNTIME_DIR` is not set, you can just give up. The XDG
      specification defines no fallback value. There are defaults, but
      they vary between Linux distros or UNIX-like systems in general.†

The next code block references these rules with comments like <code>// ②</code>.

<aside>
  <p>
    † In my case the value of <code>XDG_RUNTIME_DIR</code> just happens
    to be <samp>/run/usr/1000</samp>, though even if you could bet on
    <samp>/run/usr</samp>, you’d still have to append a textified user
    ID. I just didn’t bother, so my engine errors out at boot-up if
    <code>XDG_RUNTIME_DIR</code> is not set.
  </p>
</aside>

### Finding the Wayland socket

Here’s how i construct the Wayland socket address:
<br />
<small>
(Code compactified and asserts taken out to make it a bit more blog-friendly.)
</small>

```rs
fn socket_address_impl() -> Result<sockaddr_un, WlClientConnectError> {
  let env_wayland_display
    = unsafe { NonNull::new(getenv(c"WAYLAND_DISPLAY".as_ptr()))
                       .map(|p| CStr::from_ptr(p.as_ptr())) }
    .unwrap_or(c"wayland-0") // ①
    .to_bytes_with_nul();

  let env_xdg_runtime_dir
    = unsafe { NonNull::new(getenv(c"XDG_RUNTIME_DIR".as_ptr()))
                       .map(|p| CStr::from_ptr(p.as_ptr())) }
    .ok_or(WlClientConnectError::EnvVarXdgRuntimeDirNotSet)? // ③①
    .to_bytes();

  if env_wayland_display.first().copied() == Some(b'/') { // ②①
    fill_socket_addr(env_wayland_display.iter().copied())
  }
  else {
    let maybe_slash = match env_xdg_runtime_dir.last().copied() {
      Some(b'/') => &b"" [..],
      _          => &b"/"[..],
    };
    let path_data =      env_xdg_runtime_dir.iter().copied()   // ③
                  .chain(maybe_slash        .iter().copied())
                  .chain(env_wayland_display.iter().copied()); // ②②
    fill_socket_addr(path_data)
  }
}
fn fill_socket_addr(path: impl Iterator<Item = u8> + TrustedLen)
-> Result<sockaddr_un, WlClientConnectError> {
  let     len  = path.size_hint().0;
  let mut addr = sockaddr_un {
    sun_family: AF_LOCAL as _,
    sun_path  : [b'\0' as _; _],
  };
  if len > addr.sun_path.len() {
    return Err(WlClientConnectError::SocketOpenAddrPathTooLong);
  }
  // It annoys me that there doesn’t seem to be a better way to
  // partially fill an array from an iterator.
  addr.sun_path.iter_mut().zip(path)
      .for_each(|(l, r)| *l = r as _);
  Ok(addr)
}
```

The most spectacular thing to happen in `socket_address_impl` is me
considering the case where `XDG_RUNTIME_DIR` might not have a trailing
path separator `/`, so i should inject one myself. More noteworthy is
the implementation of `fill_socket_addr`. As you can see, i return a
`sockaddr_un` by value. That type is not exactly small, but still <1 KiB.
Further more, that type is only used for a very short time and completely
contains a valid socket path. It is implementation-defined what happens
if you write a path bigger than `sun_path` into a `sockaddr_un` and send
it off to the `socket` syscall, and implementations do vary. So i do the
safest thing and refuse to open the socket if the path is longer than
`sun_path`.

Oh, by the way… the capacity of `sun_path`? It, too, is implementation-defined.

### Connecting to the Wayland server

Now that we have our `sockaddr_un` that *hopefully* points at a UNIX
domain socket through which we can talk to our Wayland server, we can
actually allocate, open, and connect a file descriptor for that socket.

Nothing spectacular happens during that process, it’s just boring reading
of [<samp>man</samp> pages][man-connect] and wayland-client source code.
So i’ll just show you the relevant syscalls, ignoring the `errno` checks:

```rs
let wl_socket_fd = unsafe {
  socket(PF_LOCAL, SOCK_STREAM | SOCK_CLOEXEC, 0) };

unsafe { connect(
  wl_socket_fd,
  addr as *const sockaddr_un as *const _,
  // Our construction invariant is that we don’t exceed the
  // buffer size of `sun_path`. We also guarantee there’s
  // a final NUL byte. Given these invariants, we’re allowed
  // to simply pass along the struct size.
  size_of::<sockaddr_un>() as _,
)}
```

It really is as easy as that. Now we can talk to the Wayland server!

## Having a talk

From here on, we can start to follow the basic steps of the lovely
[Wayland Book][wl-book]. In fact, after this chapter, you should know
everything needed to follow the Wayland Book without relying on
wayland-client. All the function calls correspond almost 1∶1 to the
kinds of messages you’ll send and receive over the socket.

### The first request

Our first order of business as a Wayland client is to send the
`wl_display::get_registry` request†. I follow the request up with a
`wl_display::sync` request, but it’s not strictly necessary. It does
give you, however, a first taste of batching multiple requests into a
single call to `send`.

```rs
const WL_DISPLAY_ID : u32 = 0x00000001;
const WL_REGISTRY_ID: u32 = 0x00000002;

const OP_WL_DISPLAY_SYNC        : u32 = 0x000C_0000;
const OP_WL_DISPLAY_GET_REGISTRY: u32 = 0x000C_0001;

const REQUEST_WL_DISPLAY_GET_REGISTRY: [u32; 6] = [
  WL_DISPLAY_ID, OP_WL_DISPLAY_GET_REGISTRY, WL_REGISTRY_ID,
  WL_DISPLAY_ID, OP_WL_DISPLAY_SYNC        , 3,
  //          `new_id` for the sync callback ↑
];

// I built myself a more high-level socket FD type,
// but inside this method all that happens is the
// `send` syscall you’ve already seen above in the
// chapter »Is it necessary?«, plus error handling.
let bytes_sent = wl_sock_fd.send_blocking(
  &REQUEST_WL_DISPLAY_GET_REGISTRY[..]
)?;
```

That’s just 24 bytes of constant data to say hello to the Wayland server.
Or 12 bytes if you don’t add the sync request. I recommend creating an
extra module for just all these protocol constants. They do come in handy.

<aside>
  <p>
    † In practice, <code>wl_display::get_registry</code> is always the
    first request, resulting in <code>wl_registry</code> always being
    bound to ID <code>2</code>. There’s just not much else to do.
    So while the Wayland protocol <em>could</em> declare that request
    redundant and permanently assign that ID, having to do that request
    anyway has the at least theoretical benefit of serving as a
    »Wayland client magic number«. A neat little way for the server to
    quickly decide whether we’re a legitimate Wayland client or just
    a fuzzer piping <samp>/dev/random</samp> into the socket.
  </p>
</aside>

### Preparing for events

I was dead set on making it as far as i can without a single heap
allocation. To that end, it was clear that i wanted to have a statically
allocated `recv` buffer, be it on the stack or in [<samp>.bss</samp>][w-bss].
I would also have my event types *borrow* any dynamically-sized data right
from that very buffer instead of copying it… somewhere.

That begs a question, however:
<br />
<q>How big does the <code>recv</code> buffer have to be?</q>

Here’s a reminder from the [wire format specification][wl-fmt]:

<figure>
  <blockquote>
    <p>
      The second [word of a message] has 2 parts of 16-bit.
      <em>The upper 16-bits are the message size in bytes</em>,
      starting at the header […]
    </p>
  </blockquote>
</figure>

Given that we always pad messages up to an integer multiple of 4 bytes
in size, that means the largest possible Wayland message is 64 KiB big.
Unless you’re somehow running a Wayland compositor on a tiny µcontroller,
that’s really not a lot†. You can easily put that on the stack, you can
put it into a `static` variable if you prefer. There’s no need to
heap-allocate that. And so, if we have a `recv` buffer of at least 64 KiB,
or 16 384 words, we are guaranteed that a single Wayland message will always
fit inside completely. This in turn also means our message types can always
borrow dynamically-sized arguments as a simple slice.

In its most basic form, i have a Wayland event type that looks like this:

```rs
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct WlEvent<'buf> {
  pub obj : u32,
  pub op  : u16,
  pub args: &'buf [u32],
}
```

I have similar types for more specific events, along with conversion
methods from a `WlEvent`:

```rs
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub struct WlRegistryGlobal<'buf> {
  pub name     : u32,
  pub interface: &'buf str,
  pub version  : u32,
}

impl<'buf> TryFrom<WlEvent<'buf>> for WlRegistryGlobal<'buf> {
  type Error = FromWlEventError;

  fn try_from(ev: WlEvent<'buf>) -> Result<Self, Self::Error> {
    if ev.obj != WL_REGISTRY_ID {
      return Err(FromWlEventError::WrongSender);
    }
    if ev.op != EV_WL_REGISTRY_GLOBAL {
      return Err(FromWlEventError::WrongOpcode);
    }
    match ev.args {
      [name, interface_len, interface @ .., version] => {
        Ok(Self {
          name     : *name,
          interface: str_from_message(*interface_len, interface)?,
          version  : *version,
        })
      },
      _ => Err(FromWlEventError::IncorrectAmountOfArgs),
}}}
```

I have this ugly helper function for extracting UTF-8 text from
Wayland string arguments. There’s nothing special going on inside.
Just a bounds check followed by a UTF-8 check.

```rs
fn str_from_message<'buf>(message_len: u32, message: &'buf [u32])
-> Result<&'buf str, FromWlEventError>
{
  let mrange  = message.as_ptr_range();
  let mrange  = (mrange.start as *const u8)
             .. (mrange.end   as *const u8);
  let message = unsafe { slice::from_ptr_range::<'buf, u8>(mrange) };
  let message
    = match message.split_at_checked(message_len as usize) {
      Some((i, _pad)) => i,
      None            => return Err(FromWlEventError::StringArgBadLen),
    };
  let message = CStr::from_bytes_until_nul(message)
              .map_err(|_| FromWlEventError::StringArgNoNUL)?
              .to_str()
              .map_err(|_| FromWlEventError::StringArgNotUtf8)?;
  Ok(message)
}
```

<aside>
  <p>
    † Let’s have a lil’ fun with the 64 KiB buffer for a moment. The game
    »Super Mario Bros.« for the NES was about 32 KiB in size. Enough to
    completely fit inside of our buffer, with enough room to also fit
    the NES’s 2 KiB of WRAM, 2 KiB VRAM, 256 B OAM, and 28 B palette RAM.
    In short, you can fit SMB <em>and</em> a NES emulator’s game state
    inside 64 KiB! However, 64 KiB also just about fits a 256×256 px
    texture, or about 400 ms of uncompressed stereo audio. In the grand
    scheme of asset data of even modern retro-like games, 64 KiB truly is
    nothing. Still, it’s worth it every now and then to humble oneself for
    a moment and consider just how much that amount once was. And by the
    end of the day, noöne forces you to use that 64 KiB buffer <em>only</em>
    for polling Wayland events.
  </p>
</aside>

### Receiving data

In order to decode a sequence of Wayland events, we’re splitting our
buffer into 3 sections:

- Already processed, or »consumed«, event data.
- Unprocessed event data.
- Scratch space for reading more data.

Whenever we decode a Wayland event, we start reading unprocessed data.
If we hit the end of that segment before having read the full event,
we read more words from the Wayland socket into the available scratch
space and try again. If we hit the end of the entire buffer, we move
all unprocessed data towards the start, which removes all the data we
already processed and also increases the available amount of scratch
space. To help manage these operations, i created myself a special
buffer type:

```rs
struct WlBuf<'buf> {
  // This is the at least 64KiB I/O buffer we borrow.
  buf : &'buf mut [u32],

  // This range locates the unprocessed data segment
  // within `buf`. Processed data is in the range
  // `.. data.start`, scratch space is in `data.end ..`.
  data: Range<usize>,
}

impl<'buf> WlBuf<'buf> {
  // `f` reads more data into our scratch space, then tells
  // us how much data was read, so we can accordingly grow
  // our unprocessed data segment.
  pub fn fill_spare_capacity<F>(&mut self, f: F)
  -> Result<usize, SocketFdIoError>
  where F: FnOnce(&mut [u32]) -> Result<usize, SocketFdIoError> {
    let words_read = (f)(&mut self.buf[self.data.end ..])?;
    self.data.end += words_read;
    Ok(words_read)
  }

  // A Wayland event tells us how big it is. This helper method will
  // mark that event’s data as processed… if the size is in bounds.
  pub fn try_consume(&mut self, n: usize) -> Result<&[u32], ()> {
    if let Some((consumed, _rest))
     = (&self.buf[self.data.clone()]).split_at_checked(n){
      self.data.start += n;
      Ok(consumed)
    }
    else { Err(()) }
  }

  // Move unprocessed data to the start, growing the scratch space.
  pub fn shift_down(&mut self) {
    if (self.unprocessed_data().len() > 0)
     & (self.data.start               > 0) {
      self.buf.copy_within(self.data.clone(), 0);
}}}
```

As our next ingredients, we actually need to read data words from the
Wayland socket in a way we can plug into `fill_spare_capacity`. It’s
pretty straight-forward:

```rs
impl SocketFd {
  // The `blocking` parameter will come in handy later. It allows us
  // to distinguish between more data available »right now« and there
  // being no further events to process.
  pub fn recv_words<'buf>(&mut self, buf: &'buf mut [u32], blocking: bool)
  -> Result<usize, SocketFdIoError>
  {
    let c_buf_len = buf.len() * size_of::<u32>();
    let c_buf_ptr = buf.as_mut_ptr() as *mut c_void;
    let msg_flags = if blocking { 0 } else { MSG_DONTWAIT };

    match unsafe { recv(self.0, c_buf_ptr, c_buf_len, msg_flags) } {
      bytes_sent @ 0.. => {
          let bytes_sent = bytes_sent as usize;
          let words_sent = bytes_sent / size_of::<u32>();

          if bytes_sent % size_of::<u32>() != 0 {
            // This is the »out of sync« case i mentioned earlier.
            Err(SocketFdIoError::IncompleteDataFromServer)
          }
          else {
            buf
              .split_at_mut_checked(words_sent)
              .map(|(filled, _unused)| filled.len())
              // It would be quite the serious kernel bug if this happened.
              .ok_or(SocketFdIoError::Mystery)
          }
        },
      _err => match unsafe { *__errno_location() } {
        | EWOULDBLOCK
        | EAGAIN
          => Ok(0),
        _ => … more errno handling…,
      },
    }
  }

  // A pretty straight-forward helper. Tells us whether any new data
  // has been received at all.
  fn shift_down_and_read_more(&mut self, buf: &mut WlBuf<'_>, blocking: bool)
  -> Result<bool, SocketFdIoError> {
    buf.shift_down();
    buf.fill_spare_capacity(|buf| self.recv_words(buf, blocking))
       .map(|n| n > 0)
}}
```

With these tools we can build the basic structure of our Wayland event
loop pump:

```rs
impl SocketFd {
  // For once, i left actual comments from my code in.
  pub fn poll_events<'buf, F>(&mut self, buf: &mut WlBuf<'buf>, mut f: F)
  -> Result<(), SocketFdIoError>
  where F: FnMut(WlEvent<'_>)
  {
    loop {
      match Self::try_fetch_event(buf) {
        Ok(Some(ev)) => (f)(ev),

        // Non-blocking because we might be done processing events for now.
        Ok(None) => if !self.shift_down_and_read_more(buf, false)? {
          return Ok(())
        },
        Err(SocketFdIoError::OutOfMemory(Some(_))) => {
          // Blocking because we need this data *now* in order to finish
          // processing the data we already have.
          if !self.shift_down_and_read_more(buf, true)? {
            // Reading 0 words can happen if…
            // • … the »stream socket peer has performed an orderly shutdown«.
            // • … a datagram socket (which this isn’t) sent nothing.
            // In both cases, the result is that we sit on incomplete data.
            // Another condition would be us giving `recv` a 0-length buffer.
            return Err(SocketFdIoError::IncompleteDataFromServer);
          }
        },
        Err(e) => return Err(e),
}}}}
```

Don’t worry, you’ll see `try_fetch_event` soon. For now, though, let’s
focus on what we’re doing in `poll_events`. Within the event pump loop,
we try to decode one Wayland event at a time. If we got one, we forward
it to the caller. `try_fetch_event` only looks at our *unprocessed* data.
If that segment is empty, it’ll tell us there are no more events. That’s
when we receive `Ok(None)`. When `try_fetch_event` complains about being
out of memory, we know there’s not enough unprocessed data available to
decode the current event and `shift_down_and_read_more` event data. In
the special `Ok(None)` case we do one last non-blocking attempt to
`shift_down_and_read_more`, however. This is due to the unlikely
possibility that our last event was just perfectly sized such that our
unprocessed data segment is now empty, although there’s still more data
waiting for us in the socket.

### Decoding events

Fully decoding a Wayland event can be done in two steps, which i highly
recommend you do, too. Step ① is to decode event data into the `WlEvent`
type shown before, which does no interpretation of event arguments
whatsoever. That’s what happens in this function:

```rs
impl SocketFd {
  fn try_fetch_event<'p>(buf: &'p mut WlBuf<'_>)
  -> Result<Option<WlEvent<'p>>, SocketFdIoError>
  {
    match buf.len() {
      0 => return Ok(None),
      // All Wayland messages are at least 2 words long. So we tell
      // our caller we expected to see at least 2 unprocessed words.
      1 => return Err(SocketFdIoError::OutOfMemory(NonZeroU16::new(2))),
      _ => (),
    }
    let unprocessed = buf.unprocessed_data();
    let obj         = unprocessed[0];
    let len_and_op  = unprocessed[1];
    let len         = (len_and_op >>     16) as usize;
    let op          = (len_and_op &  0xFFFF) as u16;
    let len_words   = len / size_of::<u32>();

    if len % size_of::<u32>() != 0 {
      return Err(SocketFdIoError::IncompleteDataFromServer);
    }
    if let Ok(raw_event_words) = buf.try_consume(len_words) {
      let args = &raw_event_words[2..];
      return Ok(Some(WlEvent { obj, op, args }));
    }
    else {
      return Err(SocketFdIoError::OutOfMemory(
        NonZeroU16::new(len_words as u16)
      ));
}}}
```

And that’s it. We got a `WlEvent` pump now! And one that distrusts the
connected-to Wayland server at that. We check all the wire format
constraints before attempting to make sense of the data we got.

In the chapter »Preparing for Events«, we’ve already seen an example
of how to convert a `WlEvent` into a higher-level special event type
like `WlRegistryGlobal`, which also covered how to decode text arguments.
In general, this is how *all* event decoding works. There’s just one
caveat, into which we’ll look later:

Converting a `WlEvent` into any higher-level events requires that you
know the *current* ID of the object that sent them. For `wl_display`
and `wl_registry` these are known constants, sure, but all other IDs
are only known at runtime.

However, what we have now already suffices to decode our first
Wayland server responses.

### The first events

It’s been a while since we sent our `REQUEST_WL_DISPLAY_GET_REGISTRY`
to the Wayland server. Time to see what the server replied with.

```rs
// For now, let’s just say that `wait_events_impl` calls our
// `SocketFd::poll_events` in blocking mode.
_ = WlClient::wait_events_impl(
  &mut wl_sock_fd,
  &mut wl_globals,
  &mut read_buf[..],
  |ev| {
    if let Ok(ev) = WlRegistryGlobal::try_from(ev) {
      println!("{ev:?}");
    }
    else if let Ok(ev) = WlError::try_from(ev) {
      println!("{ev:?}");
    }
    else {
      println!("{ev:?}");
    }
  },
)?;
```

The above code snippet reads data from our `wl_sock_fd`, decodes
`WlEvent`s in a loop, and we just debug-`println!` them to see what
we got. In my case i receive over 60 events from the Wayland server,
adding up to just under 4 KiB of data received.
Here’s an excerpt:

<pre><samp>…
WlRegistryGlobal { name: 9, interface: "wl_shm", version: 1 }
…
WlRegistryGlobal { name: 51, interface: "zwp_text_input_manager_v1", version: 1 }
WlRegistryGlobal { name: 52, interface: "zwp_text_input_manager_v2", version: 1 }
WlRegistryGlobal { name: 53, interface: "zwp_text_input_manager_v3", version: 1 }
…
WlEvent(@0x00000003, op=0, args: [0x00157C25])
WlEvent(@0x00000001, op=1, args: [0x00000003])
</samp></pre>

We get a long list of `WlRegistryGlobal` events, and finish off with
two… other ones. Let’s have a look at the very last event first. It
was sent by object `1`, which we know is always `wl_display`. Now if
we check the documentation for event `1` of `wl_display`, we get
[`wl_display::delete_id`][wl-del]. It’s a server-side confirmation that
the object behind ID `3` was deleted and that ID `3` is available for
reüse by our client.

If you went back to the first code snippet in the chapter »The first Request«,
we sent a `wl_display::sync` request with the `new_id` argument set to `3`.
And before that, in »The XML Schema«, i referred to `3` as `next_free_id`.
So what happened is that we picked the next free ID, which just happened
to be `3`†, for the callback event of our `wl_display::sync` request, and
as the request was handled, the server already deleted the corresponding
callback object of ID `3`, meaning we can assign ID `3` again to some
other `new_id`.

Now if we look at the second last event, it was sent by object `3`,
which is the immediately deleted callback to our sync request. Its
argument, here `0x00157C25`, is of no interest to us. For all we care,
it’s just a random number without meaning. The important take-away here
is this, however:

<section class="box">
  We received a sequence of two events acknowledging our sync request,
  i.e. we <em>know</em> we aren’t missing any further Wayland events.
</section>

While my game engine code *currently* ignores these sync responses, being
able to see and debug-`print` them can be helpful while, well, debugging.
You, however, may utilise them for all kinds of things, like waking up
`async` tasks identified by their callback IDs.

<aside>
  <p>
    † ID <code>0</code> is Wayland’s <code>NULL</code>, ID <code>1</code>
    is always the <code>wl_display</code> singleton, and in most cases,
    including this blog post, ID <code>2</code> is <code>wl_registry</code>.
    And so we start counting <code>next_free_id</code> at <code>3</code>.
  </p>
</aside>

## Handling globals

We’re now ready to perform our first real exchange between our client
and the Wayland server. If you follow the Wayland Book, we’ve just
made it to [chapter 6, »Buffers & surfaces«][wl-book-6]. Here’s that
chapter’s code example. While it’s written and C, and while it uses
the wayland-client library, we’ll be doing something quite similar.

```c
// Book and code by Drew DeVault, CC-BY-SA-4.0
struct our_state {
    // ...
    struct wl_compositor *compositor;
    // ...
};

static void
registry_handle_global(void *data, struct wl_registry *wl_registry,
		uint32_t name, const char *interface, uint32_t version)
{
    struct our_state *state = data;
    if (strcmp(interface, wl_compositor_interface.name) == 0) {
        state->compositor = wl_registry_bind(
            wl_registry, name, &wl_compositor_interface, 4);
    }
}

int
main(int argc, char *argv[])
{
    struct our_state state = { 0 };
    // ...
    wl_registry_add_listener(registry, &registry_listener, &state);
    // ...
}
```

In short, we’ll need to store metadata on interfaces like `wl_compositor`
we’d like to remember and later bind. That metadata is an available
interface’s »name« <small>(Just another `u32` that shan’t be `0`.)</small>
and the ID we decide to `wl_registry::bind` it to.

In the previous chapter, you may have wondered what that mysterious
`wl_globals` parameter to `wait_events_impl` was. Well, it’s my equivalent
of the book’s `struct our_state`.

### Preparing to remember things

To design the `wl_globals` type, we need to know a few things:

- Which interfaces are we actually interested in?
- Which interface versions do we support?
- What’s the `next_id` we can bind a new global to?

First, let’s discuss a few of the Wayland interfaces i decided to
bind. Note that there’s no guarantee any of these interfaces will
be available for the application. Here’s my current selection:

- [`wl_compositor`][wl_com], without which we cannot create a
  `wl_surface`, aka. a window.
- [`wl_seat`][wl_seat], without which we don’t get mouse, keyboard,
  or touch input events.
- [`xdg_wm_base`][wm_base], which allows us to play along nicely
  with the host system.
  - For example, it has `ping/pong` messages with which the host system
    can tell if our app freeses.
- [`xdg_decoration_manager`][xdg_decor], with which we can ask the host
  system to draw a nice window border for us, with title bar and buttons
  and all.
- [`wp_content_type_manager`][wp_content], which isn’t a WordPress plugin,
  but a protocol with which we can tell the compositor that we’re a game,
  allowing for some potential optimisations in the Wayland compositor.

As for the interface versions, i just copy-pasted them out from my
`WlRegistryGlobal` debug-`println!`s. It’ll take a few years before my
project is finished, so while the versions i picked are the hot new
shit *now*, years later they’ll be ancient history. The take-away is:
Feel free to pick whichever versions make sense to you. Here’s the
version constants i went with:

```rs
const V_WL_COMPOSITOR          : u32 = 6;
const V_WL_SEAT                : u32 = 9;
const V_XDG_WM_BASE            : u32 = 6;
const V_XDG_DECORATION_MANAGER : u32 = 1;
const V_WP_CONTENT_TYPE_MANAGER: u32 = 1;
```

Well, and finally we need our running `next_id` counter starting at `3`.
Combining all that, we get:

```rs
struct WlGlobals {
  pub next_id: u32,

  pub wl_compositor          : WlGlobal,
  pub wl_seat                : WlGlobal,
  pub xdg_wm_base            : WlGlobal,
  pub xdg_decoration_manager : WlGlobal,
  pub wp_content_type_manager: WlGlobal,
}
impl WlGlobals {
  pub fn new() -> Self {
    Self {
      next_id                : 3,
      wl_compositor          : WlGlobal::default(),
      wl_seat                : WlGlobal::default(),
      xdg_wm_base            : WlGlobal::default(),
      xdg_decoration_manager : WlGlobal::default(),
      wp_content_type_manager: WlGlobal::default(),
}}}

#[derive(Default)]
struct WlGlobal {
  pub name: Option<NonZeroU32>, // default: None
  pub id  : Option<NonZeroU32>,
}
```

Note that instead of using bare `u32`s here, i went with the semantically
better fitting `Option<NonZeroU32>`. A `NULL` object is represented as
`None` now, allowing us to properly distinguish a few important cases:

```rs
impl fmt::Debug for WlGlobal {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match (self.name, self.id) {
      (None   , _      ) => write!(f, "WlGlobal(None)"),
      (Some(n), None   ) => write!(f, "WlGlobal(name:{n:#010X})"),
      (Some(n), Some(i)) => write!(f, "WlGlobal(name:{n:#010X}, id:{i:#010X})"),
}}}
```

As you create new objects, like a `wl_keyboard`, you’ll find yourself
adding ever more properties to `WlGlobal`. The general way we manage
them doesn’t change, however.

### Actually remembering things

What comes next is pretty straight-forward. We build ourselves an event
handler that looks for `WlRegistryGlobal` events and then checks whether
any of these name an interface we’re interested in. Lucky for us, this
is a rare case for the Wayland protocol in which look-ups for the correct
event handler can be made fast. In the following `match` expression, the
text comparison of all the interface names like `"wl_compositor"` compiles
down to a rather quick† search tree.

```rs
impl WlGlobals {
  fn on_wl_registry_global(&mut self, ev: WlRegistryGlobal<'_>) -> bool {
    let name = NonZeroU32::new(ev.name);
    // We only need one of each of our globals. Some like `wl_output`
    // can appear multiple times, for different purposes. That’s why
    // we check for `is_none`.
    match (ev.interface, ev.version) {
      ("wl_compositor", v)
      if v >= V_WL_COMPOSITOR
      && self.wl_compositor.name.is_none() => {
         self.wl_compositor.name = name;
        true
      },
      ("wl_seat", v) …
      ("xdg_wm_base", v) …
      ("zxdg_decoration_manager_v1", v) …
      ("wp_content_type_manager_v1", v) …
      _ => false,
}}}
```

For most other Wayland things, we annoyingly have to perform a linear
search over every one of our globals in order to correctly handle an
event, simply because Wayland messages do not include a type hint‡ for
what *interface* sent an event. Alternatively, one could build hash tables
of object IDs and dynamically dispatched event handling closures. Both
designs are rather ugly. I went with the linear search technique, for
while it has <samp>O(n)</samp> time complexity, our <samp>n</samp> is
small and it needs no dynamic dispatch or dynamic memory allocation.

<aside>
  <p>
    † The search can theoretically be made a wee bit faster by not
    matching over <code>&str</code> but instead matching over the raw
    <code>&[u32]</code>. However, that small speed bost would come at
    the cost of making the code much harder to read, for events that
    only occur rarely.
  </p>
  <p>
    ‡ To cut Wayland some slack, it’s really difficult to add type hinting
    to the protocol, apart from the whole »breaking changes« thing. The
    way Wayland is designed <em>now</em>, such a type hint would have to
    be at least the interface name. Imagine every
    <code>wl_registry::global</code> was prefixed with
    <code>"wl_registry"</code>. That’s an additional 16 bytes per event.
    And that metadata becomes ever more expensive the longer the interface
    name. Just look at <code>zxdg_decoration_manager_v1</code>, for example.
    A likely consequence would be renaming these interfaces like it’s the
    70s again: <code>zxdgdmgr1</code>, beautiful, and still 16 bytes.
    The only reasonable alternative would be to use hashed interface names.
    For example, <code>"wl_registry"</code> would become
    <code>xxHash32("wl_registry") = 1AE18F67₁₆</code>. That’s nice, all
    interface names become integer constants, but we can always derive
    these constants from text at compile time. However, this requires
    that at least all major protocol extensions »register« themselves
    in some central document, so that we can guarantee there’s no hash
    collisions between them.
  </p>
</aside>

### Forgetting things

We’ll soon be »binding« globals, which means we should prepare for
properly cleaning up behind ourselves. So far, the only requests we’ve
sent to the Wayland server consisted of just constants. Now we’re about
to generate our first more involved sequence of requests, for these
depend on runtime data.

The primary motivation for this whole endeavour is that i know exactly
what kind of application i’m building, and that i can thus utilise a
lot of domain-specific knowledge. In this here case, i know exactly how
many disposable objects i have *at most*, as well as how to properly
get rid of them. So let’s do that first: Calculate the maximum buffer
size we need to serialise all delete requests we’ll ever need.

```rs
// The amount of bits we need to shift an opcode+length
// by to extract the length divided by 4.
const OP16_W2: u32
  = u16::BITS
  + size_of::<u32>().trailing_zeros(); // aka. log₂(4)

const OP_WL_SEAT_RELEASE_LEN: usize
= OP_WL_SEAT_RELEASE as usize >> OP16_W2;

const OP_XDG_WM_BASE_DESTROY_LEN: usize
= OP_XDG_WM_BASE_DESTROY as usize >> OP16_W2;

const OP_XDG_DECORATION_MANAGER_DESTROY_LEN: usize
= OP_XDG_DECORATION_MANAGER_DESTROY as usize >> OP16_W2;

const OP_WP_CONTENT_TYPE_MANAGER_DESTROY_LEN: usize
= OP_WP_CONTENT_TYPE_MANAGER_DESTROY as usize >> OP16_W2;

const DROP_MSG_LEN: usize
    = OP_WL_SEAT_RELEASE_LEN
    + OP_XDG_WM_BASE_DESTROY_LEN
    + OP_XDG_DECORATION_MANAGER_DESTROY_LEN
    + OP_WP_CONTENT_TYPE_MANAGER_DESTROY_LEN;
```

The result for this initial set of deletable objects is… 8 words. So
using just a 32 bytes buffer on the stack, we can serialise all possible
object deletion requests. Note that `wl_compositor` doesn’t appear in
that list, although we had to `wl_registry::bind` it. The compositor is
special in that it is an undisposable singleton object. For every other
deletable object you add to your list of globals, just follow the above
pattern.

With this we can now implement `Drop` for our `WlGlobals`:

```rs
impl Drop for WlClient {
  fn drop(&mut self) {
    let mut buf = [0_u32; DROP_MSG_LEN];

    let words_written = buf.len() - {
      let mut buf = &mut buf[..];

      if let Some(global_id) = self.wl_globals.wl_seat.id {
        buf[0] = global_id.get();
        buf[1] = OP_WL_SEAT_RELEASE;

        buf = &mut buf[OP_WL_SEAT_RELEASE_LEN ..];
      }
      if let Some(global_id) = self.wl_globals.xdg_wm_base.id {
        buf[0] = global_id.get();
        buf[1] = OP_XDG_WM_BASE_DESTROY;

        buf = &mut buf[OP_XDG_WM_BASE_DESTROY_LEN ..];
      }
      … same pattern …

      buf.len()
    };
    if words_written > 0 {
      let _ = self.wl_sock_fd.send_blocking(&buf[.. words_written]);
    }
}}
```

Note that with this being the `Drop` implementation, we don’t have to
bother with clearing our freshly deleted state.

You may be tempted to further reduce this pattern into a loop, based
on a few key observations:

- It just so happens that all deletion requests so far are 2 words in size.
- It just so happens that all `::destroy` requests have the opcode `0`.

Beware, however. Make sure these assumptions really hold true for all
disposable objects you bind. For example, `wl_seat::release` has the
opcode `3`. It’s not really a destructor and more of a hint to the
compositor that we aren’t going to handle user input anymore.

Apart from that, whether you refactor that code into a loop or not, make
sure you dispose of objects in the correct order when you have objects
created with other objects. For example, destroy a `wl_keyboard` before
destroying a `wl_seat`.

### Trying harder to forget things

The above code is lovely and all for the ideal case where we only have
to bother with resource clean-up during app shut-down. There is, however,
a Wayland event spoiling our naïve fun:
[`wl_registry::global_remove`][wl-remove]

One reason such an event might pop up could be that the user’s bluetooth
mouse ran out of battery. Another reason might be that the keyboard
cable got pulled out by accident. Or the Wayland server just felt like
bullying us special snowflake. Whatever the reason might be, when the
server tells us that a global disappeared, it is our responsibility to
properly call the corresponding object’s destructor.

As discussed in the chapter before, you can use a loop to find the objects
you wish to get rid of. That’s what i’m doing in this event handler’s
code. I have no preferrence for either pattern, you go pick the pattern
you find the easiest to extend.

```rs
fn on_wl_registry_global_remove(
  &mut self,
  fd: &mut SocketFd,
  ev: WlRegistryGlobalRemove
)
-> Result<bool, WlClientPollError> {
  let name = NonZeroU32::new(ev.name);
  // No destructor to call, just forget about the global.
  // Super unlikely this `if` will ever test `true`.
  if self.wl_compositor.name == name {
      self.wl_compositor = WlGlobal::default();
    return Ok(true);
  }
  // This time i’m making use of the commonalities between deletion
  // requests. Who knows how this code’ll change if i encounter a
  // »non-standard« destructor.
  let globals = [
    (&mut self.wl_seat                , OP_WL_SEAT_RELEASE                ),
    (&mut self.xdg_wm_base            , OP_XDG_WM_BASE_DESTROY            ),
    (&mut self.xdg_decoration_manager , OP_XDG_DECORATION_MANAGER_DESTROY ),
    (&mut self.wp_content_type_manager, OP_WP_CONTENT_TYPE_MANAGER_DESTROY),
  ];
  for (global, op) in globals {
    if global.name == name {
      // We might’ve not yet bound this global. So only delete if needed.
      if let Some(id) = global.id {
        fd.send_blocking(&[id.get(), op])?;
      }
      *global = WlGlobal::default();
      return Ok(true);
    }
  }
  Ok(false)
}
```

Yes, unlike `on_wl_registry_global`, this function takes a `SocketFd`
through which i can immediately send a deletion request. You could
decide to queue up deletion requests for later and send them in one go,
be it by writing the `WlGlobal` metadata into a list, or by writing the
deletion requests into a buffer first. I went with the much simpler,
though arguably messier, and less I/O-efficient option of sending out
the deletion requests immediately. I’m gambling on `WlRegistryGlobalRemove`
events being rare and on them ganging up being even rarer.

### Global registering and deleting done

Remember the call to `WlClient::wait_events_impl` in the chapter
»The first events«? Internally, it calls this here function:

```rs
impl WlClient {
  fn poll_or_wait_events_impl<'buf, F>(
    fd      : &mut SocketFd,
    globals : &mut WlGlobals,
    buf     : &'buf mut [u32],
    blocking: bool,
    mut f   : F,
  )
  -> Result<(), WlClientPollError>
  where F: FnMut(&mut SocketFd, WlEvent<'_>) -> Result<(), WlClientPollError>
  {
    let mut buf  = WlBuf::<'buf>::new(buf);

    if blocking {
      // Pre-fill the buffer blockingly if blocking event pumping is
      // requested. Easier to do than parameterising `poll_events`.
      buf.fill_spare_capacity(|buf| fd.recv_words(buf, true))?;
    }
    fd.poll_events(
      &mut buf,
      |fd, ev| {
        if !globals.maybe_handle_event(fd, ev)? {
          (f)(fd, ev)?;
        }
        Ok(())
      },
    )
}}
```

Our `maybe_handle_event` call does what you might expect. If it’s an
event for management of globals, we consume and handle it. Otherwise
we forward the event to `f`. We pass along `fd` for similar cases like
`wl_registry::global_remove`, where we want to respond to events
immediately rather than later. Though as previously discussed, gathering
replies to send them *after* pumping events is the cleaner approach. I
may get rid of this `fd` forwarding one day, who knows.

Now let’s have a taste of `maybe_handle_event` and how easy things could
be if only we had proper type hints. Because your other event handling
code will not be as simple as that.

```rs
impl WlGlobals {
  pub fn maybe_handle_event(&mut self, fd: &mut SocketFd, ev: WlEvent<'_>)
  -> Result<bool, WlClientPollError> {
    match (ev.obj, ev.op) {
      (WL_REGISTRY_ID, EV_WL_REGISTRY_GLOBAL)
      => Ok(self.on_wl_registry_global(ev.try_into()?)),

      (WL_REGISTRY_ID, EV_WL_REGISTRY_GLOBAL_REMOVE)
      => self.on_wl_registry_global_remove(fd, ev.try_into()?),

      _ => Ok(false),
    }
  }
}
```

That’s it. Because our `wl_registry`’s ID is constant, we can perform
this nice and deterministic branch over `(ID, opcode)` to decode and
then handle our event. For other objects we’ll have to run a search loop.

### One call to bring them all and in WlGlobals bind them

Deleting globals we bound is nice and all, but that code won’t do anything
until we actually do the binding. So that’s what we shall do here.

This next function follows the general architecture of the `Drop`
implementation in the chapter »Forgetting things«. We repeatedly write
data into a slice and reslice it towards the end after each step. We
even statically know the maximum amount of data we’ll send.

This function is called right after we pumped our first Wayland events
in »The first events«. For each global that has a name but no bound ID
yet, we write a [`wl_registry::bind`][wl-bind] request. Remember
»the new_id pitfall«? This request is one of these pitfalls. The
documentation reads like it takes `(name, new_id)` as arguments, though
really it takes `(name, interface_name, interface_version, new_id)`.

```rs
impl WlClient {
  fn bind_new_globals(
    wl_globals: &mut WlGlobals,
    fd        : &mut SocketFd,
  )
  -> Result<(), SocketFdIoError> {
    const MAX_GLOBALS: usize = 5; // ← This should be unnecessary.

    let next_id = &mut wl_globals.next_id;

    let candidate_globals: [_; MAX_GLOBALS] = [
      (&mut wl_globals.wl_compositor          , IV_WL_COMPOSITOR),
      (&mut wl_globals.wl_seat                , IV_WL_SEAT      ),
      (&mut wl_globals.xdg_wm_base            , IV_XDG_WM_BASE  ),
      (&mut wl_globals.xdg_decoration_manager , IV_XDG_DEC_MGR  ),
      (&mut wl_globals.wp_content_type_manager, IV_WP_CONTY_MGR ),
    ];

    let mut buf = [0_u32; MAX_GLOBALS * BIND_LEN + IV_LEN + SYNC_LEN];

    let words_written = buf.len() - {
      let mut buf = &mut buf[..];

      for (global, iv) in candidate_globals {
        if let Some(name) = global.name
        && global.id.is_none()
        {
          let extra_len = (iv.len() as u32) << OP16_W2;
          buf[0] = WL_REGISTRY_ID;
          buf[1] = OP_WL_REGISTRY_BIND + extra_len;
          buf[2] = name.get();
          buf    = &mut buf[3..];
          
          (&mut buf[..iv.len()]).copy_from_slice(iv);
          buf[iv.len()] = *next_id;
          buf           = &mut buf[(1+iv.len()) ..];

          global.id = NonZeroU32::new(*next_id);
          * next_id = match *next_id + 1 {
            id @ 0..=0xFEFFFFFF => id,
            _ => todo!("TODO recycle old IDs"),
          };
        }
      }
      buf[0] = WL_DISPLAY_ID;
      buf[1] = OP_WL_DISPLAY_SYNC;
      buf[2] = *next_id; // No post-increment, ID immediately available again.
      buf    = &mut buf[SYNC_LEN ..];
      buf.len()
    };

    if words_written > SYNC_LEN {
      let _ = fd.send_blocking(&buf[.. words_written])?;
    }
    Ok(())
}}
```

A few notable things happen in that function.

- `buf[1] = OP_WL_REGISTRY_BIND + extra_len;`:
  Ignoring the interface-related arguments, the size field of a bind
  request might be constant. All we do here is add the length of our
  extra `(interface_name: &str, interface_version: u32)` arguments.
- `MAX_GLOBALS * BIND_LEN + IV_LEN + SYNC_LEN`:
  This is the maximum amount of data we’ll send. The basic size of
  every bind request, for every global we’re interested in, plus the
  total size of all interface metadata combined, plus a final good ol’
  sync request.
- `* next_id = match *next_id + 1`:
  All the way back in »Is it necessary?« i added this little `next_free_id`
  to our sync request, claiming it’ll be easy but we’ll come back to that
  later. Well, that »later« is now. And indeed it’s pretty straight-forward,
  except for a wee detail.
  - In general, whenever we write a `new_id` argument for a request, we
    write `next_id` and then post-increment it.
  - However unlikely it may be that your app eventually runs out of ID
    space, valid Wayland client IDs only go up to `FEFF'FFFF₁₆`.
  - Handling the case where you run out of IDs is not difficult, but
    ugly: Iterate over all currently bound IDs of all your globals,
    pick the next best ID equal to none of them once you hit the
    `FEFF'FFFF₁₆` ceiling.

Now let’s take a look at the constants.

```rs
const BIND_LEN: usize = OP_WL_REGISTRY_BIND as usize >> OP16_W2;
const SYNC_LEN: usize = OP_WL_DISPLAY_SYNC  as usize >> OP16_W2;
const   IV_LEN: usize
  = IV_WL_COMPOSITOR.len()
  + IV_WL_SEAT      .len()
  + IV_XDG_WM_BASE  .len()
  + IV_XDG_DEC_MGR  .len()
  + IV_WP_CONTY_MGR .len();

const IV_WL_COMPOSITOR: &[u32] = &[
  "wl_compositor\0".len() as u32,
  u32::from_ne_bytes(*b"wl_c"),
  u32::from_ne_bytes(*b"ompo"),
  u32::from_ne_bytes(*b"sito"),
  u32::from_ne_bytes(*b"r\0\0\0"),
  V_WL_COMPOSITOR,
];
```

The `_LEN` ones are rather straight-forward. The `IV_` are our
`(interface_name: &str, interface_version: u32)` metadata, neatly
serialised into `&[u32]`s we can copy into our request buffer as
needed. Note that we have to serialise text in »native endian«,
and that we have to count in at least one `NUL` terminator for
the text length.

Congratulations!

You’ve made it all the way to successfully bind Wayland globals and
get rid of them again as needed. That means we’re done here, right?

## Receiving file descriptors

Not at all. ☺

There is one thing i’ve hidden from you so far, and that is file descriptor
arguments in Wayland events. The Wayland server sends these whenever
sizable amounts of data are to be transferred.

Let’s refactor the above code. That’ll not only teach you how to deal
with file descriptor arguments, but also how to extend all the code
written so far. For an easy real-life example, we’ll bind ourselves
a `wl_keyboard`, which will greet us with a [`wl_keyboard::keymap`][wl-km]
event, which’s second argument is a file descriptor we’re supposed to
memory-map.

Don’t worry, though. After this, we’re truly done.



[rs]: https://www.rust-lang.org/learn
[rs-winit]: https://docs.rs/winit/latest/winit/
[c-glfw]: https://www.glfw.org/
[c-sdl2]: https://www.libsdl.org/
[hhero]: https://handmadehero.org/
[wl]: https://wayland.freedesktop.org/docs/html/index.html
[wl-app]: https://wayland.app/protocols/
[wl-fmt]: https://wayland.freedesktop.org/docs/html/ch04.html#sect-Protocol-Wire-Format
[wl-oops]: https://lists.freedesktop.org/archives/wayland-devel/2017-February/033224.html
[wl-sync]: https://wayland.app/protocols/wayland#wl_display:request:sync
[man-connect]: https://man.archlinux.org/man/connect.2.en
[wl-book]: https://wayland-book.com/registry/binding.html
[w-bss]: https://en.wikipedia.org/wiki/.bss
[wl-del]: https://wayland.app/protocols/wayland#wl_display:event:delete_id
[wl-book-6]: https://wayland-book.com/surfaces/compositor.html
[wl-book-0]: https://wayland-book.com/
[wl_com]: https://wayland.app/protocols/wayland#wl_compositor
[wl_seat]: https://wayland.app/protocols/wayland#wl_seat
[wm_base]: https://wayland.app/protocols/xdg-shell#xdg_wm_base
[xdg_decor]: https://wayland.app/protocols/xdg-decoration-unstable-v1#zxdg_decoration_manager_v1
[wp_content]: https://wayland.app/protocols/content-type-v1#wp_content_type_manager_v1
[wl-remove]: https://wayland.app/protocols/wayland#wl_registry:event:global_remove
[wl-bind]: https://wayland.app/protocols/wayland#wl_registry:request:bind
[wl-km]: https://wayland.app/protocols/wayland#wl_keyboard:event:keymap
