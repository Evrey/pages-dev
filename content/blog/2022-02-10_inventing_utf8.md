+++
title       = "Inventing UTF-8"
description = "Does the UTF‑8 encoding scheme make sense? Is it the best design we can come up with? In this article i’m walking through the process of designing a UTF‑8 alternative, that… well… just magically ends up being UTF‑8. How lovely when things just make sense!"
draft       = false
updated     = 2023-02-01

[taxonomies]
categories = ["data-format"]
keywords   = ["data-format", "ascii", "utf-8", "leb128", "unary"]

[extra]
uuid = "c257d53f-be9e-5ddf-8386-ab65bf9d3af3"
code = true
+++

This blog post is the result of many discussions about the
UTF-8 and UTF-16 encoding formats. Weighing ups and downs of
either. To meaningfully compare different design decisions,
however, it is important to understand why these formats were
designed the way they were. And what better way of understanding
their designs than by reïnventing them yourself?

<!-- more -->

The challenge seems simple:

1. We start off with [ASCII][ascii], this strange and limited
   7-bit text encoding scheme with a lot of strange control
   codes barely anyone remembers.
2. We want to be able to encode all of [Unicode][unic], which
   we now know encodes a single scalar value in at most 21 bits.
3. Ideally we want to use as few bits per scalar value as reasonable.
4. We boldly assume that most common text will largely consist
   of code points in the old ASCII range, so ideally pure
   ASCII text, when encoded using our new scheme, should use
   no more memory than before.

## LEB128

For the machine our text is nothing but a sequence of integers.
So our challenge boils down to finding an optimal encoding
scheme for just one of these integers and then just encode
them all with this scheme in a sequence. Given our 4<sup>th</sup>
requirement and the 7‑bitness of ASCII, we have only *one* bit
to spare to indicate whether or not more bytes belonging to
the same integer value follow.

<dfn>LEB128</dfn>, or »Little Endian Base 128«, is a common
variable-length integer encoding scheme serving precisely
this purpose. In LEB128 every number is split up into 7‑bit
chunks <small>(2⁷=128)</small>, from lowest to highest bits.
Each of these chunks then gets an 8<sup>th</sup> tag bit that
is <samp>0</samp> for the first chunk of an integer and
<samp>1</samp> for all immediately following chunks belonging
to that same integer.

Here’s an example of how different numbers are encoded in
LEB128:

<table>
  <caption>
    LEB128 tag bits are written in black with underscores.
    Note that in the first example the leading zero is discarded,
    as it would have been part of a redundant all-zero chunk.
    This property allows us to encode ASCII text using the
    exact same number of bytes, indeed even the very same
    bit pattern.
    <br>
    <small>(Dear screen-reader visitors: I tried labelling
    the chunks for you, hope it works well.)</small>
  </caption>
  <thead>
    <tr>
      <th>Decimal</th>
      <th>Binary (BE)</th>
      <th>LEB128</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>41</td>
      <td>
        <samp>
                    0<span class="b" aria-label="(blue bits)">010’1001</span>
        </samp>
      </td>
      <td>
        <samp>
          <u>0</u><span class="b" aria-label="(blue bits)">010’1001</span>
        </samp>
      </td>
    </tr>
    <tr>
      <td>297</td>
      <td>
        <samp>
                  <span class="g" aria-label="(green bits)">1 0</span><span class="b" aria-label="(blue bits)">010’1001</span>
        </samp>
      </td>
      <td>
        <samp>
          <u>0</u><span class="b" aria-label="(blue bits)">010’1001</span>
          <u>1</u><span class="g" aria-label="(green bits)">000’0010</span>
        </samp>
      </td>
    </tr>
    <tr>
      <td>809</td>
      <td>
        <samp>
                 <span class="r" aria-label="(red bits)">11 0</span><span class="b" aria-label="(blue bits)">010’1001</span>
        </samp>
      </td>
      <td>
        <samp>
          <u>0</u><span class="b" aria-label="(blue bits)">010’1001</span>
          <u>1</u><span class="r" aria-label="(red bits)">000’0110</span>
        </samp>
      </td>
    </tr>
    <tr>
      <td>33577</td>
      <td>
        <samp>
          <span class="g" aria-label="(green bits)">10</span><span class="r" aria-label="(red bits)">00’0011 0</span><span class="b" aria-label="(blue bits)">010’1001</span>
        </samp>
      </td>
      <td>
        <samp>
          <u>0</u><span class="b" aria-label="(blue bits)">010’1001</span>
          <u>1</u><span class="r" aria-label="(red bits)">000’0110</span>
          <u>1</u><span class="g" aria-label="(green bits)">000’0010</span>
        </samp>
      </td>
    </tr>
  </tbody>
</table>

Sure, this format can waste an unfortunate amount of bits when
the last chunk of an integer consists of mostly padding. However,
for all other chunks this encoding scheme is pleasantly efficient.
Awesome! So we’re done here, right?

## … or not?

The format above clearly works well enough for
the purpose of encoding arbitrary Unicode scalar values using
as few bytes as reasonable. However, how well does it actually
hold up when confronted with the real world? The world where
a ton of software is and was written in very old versions of
<abbr title="the C programming language">C</abbr>, where text
files are not just read but also written by humans, edited,
inserting and deleting random sections of text somewhere in
the middle of files.

The observant among y’all have already noticed that the
example numbers in the table above are not really random
— They were carefully selected! If you look closely at the
**LEB128 column**, you’ll notice that all blue chunks are equal.
So are all red chunks, and all green chunks. If you were to
delete the green chunk of <samp>33577</samp> you get
the LEB128-encoding of <samp>809</samp>, and if you delete
the red chunk you get <samp>297</samp>. Delete both red
and green and you get <samp>41</samp>. *Deleting a random
byte in the middle is an easy to make mistake in software.*
Especially in a language with text handling as error-prone as C’s.
It will happen somewhere. Even the opposite could easily
occur: Randomly inserting bytes where they don’t belong,
such as falsely turning <samp>41</samp> into <samp>297</samp>
by appending the green chunk. Ideally, such byte injection or
removal faults should not turn characters of text into arbitrary
different characters.

We can conclude that an additional requirement is to be added
to our original list:

5. The encoding scheme should gracefully handle faulty
   injection or removal of arbitrary single bytes.
   <abbr title="id est">I.e.</abbr> we should be able to
   detect whether a scalar value is broken and we should be
   able to read the rest of the text without further errors.

## We need a counter!

A pretty robust solution would be to enumerate all chunks of
a scalar value from 0<sup>th</sup> to last. Again, due to our
first requirement the topmost <samp>0</samp>‑bit of the first
byte should indicate that this is the only byte of data for
the current number, so that our encoding scheme is a strict
superset of ASCII.

Let’s try a naïve first scheme. If the tag bit of the first
byte is <samp>0</samp>, the remaining 7 bits are all ASCII.
If, however, the tag bit is <samp>1</samp>, the next of the
top bits are a counter field, the rest is our scalar value
chunk. Here’s the tricky part, however: We need to be able to
count a total of 21 chunk bits to represent all of Unicode.
So how many bytes will we need to accumulate enough chunk
bits for the last Unicode scalar value? To make matters worse,
the more bytes we need, the more counter bits we need, and in
turn the fewer chunk bits we get, which makes us need more
bytes. Sounds confusing? Well, it is confusing. So let’s
get rid of the confusion by demonstrating a working
implementation of this scheme.

If the tag bit is <samp>1</samp>, the next three top bits are
a 3‑bit number that counts down the remaining bytes in the
sequence. The last byte has a counter value of <samp>000</samp>.
This gives us a total of 4 bits per chunk to work with,
allowing us to encode up to 24‑bit integers using this
scheme, using a total of 6 bytes. Here’s how this looks for
the number <samp>33577</samp>:

<table>
  <caption>
    The counter bits are <mark>marked</mark>. And because i
    want to keep my CSS colour palette rather small, the
    fourth chunk uses the regular text colour, i.e. black,
    unless you told your browser you prefer dark themes.
  </caption>
  <thead>
    <tr>
      <th>Decimal</th>
      <th>Binary (BE)</th>
      <th>Counter (LE, naïve)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>33577</td>
      <td>
        <samp>
          <span aria-label="(black bits)">1000</span>’<span class="g" aria-label="(green bits)">0011</span>
          <span class="r" aria-label="(red bits)">0010</span>’<span class="b" aria-label="(blue bits)">1001</span>
        </samp>
      </td>
      <td>
        <samp>
          <u>1</u><mark>011</mark>’<span class="b" aria-label="(blue bits)">1001</span>
          <u>1</u><mark>010</mark>’<span class="r" aria-label="(red bits)">0010</span>
          <u>1</u><mark>001</mark>’<span class="g" aria-label="(green bits)">0011</span>
          <u>1</u><mark>000</mark>’<span aria-label="(black bits)">1000</span>
        </samp>
      </td>
    </tr>
    <tr>
      <td>�</td>
      <td>
        <samp>
          <span aria-label="(black bits)">1000</span>’<span class="g" aria-label="(green bits)">0011</span>
          <span class="r" aria-label="(red bits)">----</span>’<span class="b" aria-label="(blue bits)">1001</span>
        </samp>
      </td>
      <td>
        <samp>
          <u>1</u><mark>011</mark>’<span class="b" aria-label="(blue bits)">1001</span>
          <u>1</u><mark>001</mark>’<span class="g" aria-label="(green bits)">0011</span>
          <u>1</u><mark>000</mark>’<span aria-label="(black bits)">1000</span>
        </samp>
      </td>
    </tr>
    <tr>
      <td>2098</td>
      <td>
        <samp>
               <span aria-label="(black bits)">1000</span>
          <span class="g" aria-label="(green bits)">0011</span>’<span class="r" aria-label="(red bits)">0010</span>
        </samp>
      </td>
      <td>
        <samp>
          <u>1</u><mark>010</mark>’<span class="r" aria-label="(red bits)">0010</span>
          <u>1</u><mark>001</mark>’<span class="g" aria-label="(green bits)">0011</span>
          <u>1</u><mark>000</mark>’<span aria-label="(black bits)">1000</span>
        </samp>
      </td>
    </tr>
    <tr>
      <td>�</td>
      <td>
        <samp>
          <span aria-label="(black bits)">----</span>’<span class="g" aria-label="(green bits)">----</span>
          <span class="r" aria-label="(red bits)">0010</span>’<span class="b" aria-label="(blue bits)">1001</span>
        </samp>
      </td>
      <td>
        <samp>
          <u>1</u><mark>011</mark>’<span class="b" aria-label="(blue bits)">1001</span>
          <u>1</u><mark>010</mark>’<span class="r" aria-label="(red bits)">0010</span>
          <u>1</u><mark>010</mark>’<span class="r" aria-label="(red bits)">0010</span>
          <u>1</u><mark>001</mark>’<span class="g" aria-label="(green bits)">0011</span>
          <u>1</u><mark>000</mark>’<span aria-label="(black bits)">1000</span>
        </samp>
      </td>
    </tr>
  </tbody>
</table>

If we were to now remove the bytes of the red, green or black
chunk, our text decoder program would detect that a numbered
byte is missing. However, something interesting happens if we
were to delete the blue chunk: It gets falsely accepted as a
different number again! We have no way of knowing whether the
red chunk was the first or a later byte of an encoded number.
It gets even weirder if we inject a byte that repeats an
adjacent chunk counter. In the last example, the red chunk
is repeated. We can detect that the first two bytes of our
encoded message form an incomplete scalar value, given that
the follow-up counter <samp><mark>001</mark></samp> is missing.
However, the last three bytes get again recognised as a valid
number, <samp>2098</samp>.

Indeed, this encoding scheme is clearly broken. But can we
potentially fix it, somehow? Well, we can try, by identifying
a crucial piece of information that this scheme is lacking.

## We need a start bit!

Let’s go back to LEB128 for a moment. We just identified
a new kind of fault in the naïve counter scheme that
cannot happen in LEB128: If we delete the first bytes of a
sequence, the remaining bytes still form a valid encoded
number! The solution for this is easy. In LEB128, a single
bit tells us whether a byte is the first of a sequence or
any of the remaining bytes.

Let’s add such a start/rest bit to our previous counter
design. That gives us 3 counter bits, 1 start/rest bit,
and 3 payload bits, allowing us to encode up to 21‑bit
integers in 7 bytes. Here’s what it looks like now:

<table>
  <caption>
    The start/stop bit is marked as <ins>0</ins>/<ins>1</ins>.
    Abusing the HTML <code>&lt;ins&gt;</code> tag, though
    technically we <em>are</em> inserting a new bit here and there.
  </caption>
  <thead>
    <tr>
      <th>Dec.</th>
      <th>Binary (BE)</th>
      <th>Counter (LE, terrible)</th>
      <th>Error</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>297</td>
      <td>
        <samp>
          <span class="g" aria-label="(green bits)">1 00</span><span class="r" aria-label="(red bits)">10’1</span><span class="b" aria-label="(blue bits)">001</span>
        </samp>
      </td>
      <td>
        <samp>
          <u>1</u><ins>0</ins><mark>010</mark><span class="b" aria-label="(blue bits)">001</span>
          <u>1</u><ins>1</ins><mark>001</mark><span class="r" aria-label="(red bits)">101</span>
          <u>1</u><ins>1</ins><mark>000</mark><span class="g" aria-label="(green bits)">100</span>
        </samp>
      </td>
      <td>All good!</td>
    </tr>
    <tr>
      <td>�</td>
      <td>�</td>
      <td>
        <samp>
          <u>1</u><ins>1</ins><mark>001</mark><span class="r" aria-label="(red bits)">101</span>
          <u>1</u><ins>1</ins><mark>000</mark><span class="g" aria-label="(green bits)">100</span>
        </samp>
      </td>
      <td>Where’s the start, mate?</td>
    </tr>
    <tr>
      <td>�</td>
      <td>
        <samp>
          <span class="g" aria-label="(green bits)">1 00</span><span class="r" aria-label="(red bits)">--’-</span><span class="b" aria-label="(blue bits)">001</span>
        </samp>
      </td>
      <td>
        <samp>
          <u>1</u><ins>0</ins><mark>010</mark><span class="b" aria-label="(blue bits)">001</span>
          <u>1</u><ins>1</ins><mark>000</mark><span class="g" aria-label="(green bits)">100</span>
        </samp>
      </td>
      <td>Something’s missing in the middle…</td>
    </tr>
    <tr>
      <td>�</td>
      <td>
        <samp>
          <span class="g" aria-label="(green bits)">- --</span><span class="r" aria-label="(red bits)">10’1</span><span class="b" aria-label="(blue bits)">001</span>
        </samp>
      </td>
      <td>
        <samp>
          <u>1</u><ins>0</ins><mark>010</mark><span class="b" aria-label="(blue bits)">001</span>
          <u>1</u><ins>1</ins><mark>001</mark><span class="r" aria-label="(red bits)">101</span>
        </samp>
      </td>
      <td>Where’s the rest?</td>
    </tr>
    <tr>
      <td>�</td>
      <td>
        <samp>
          <span class="g" aria-label="(green bits)">- --</span><span class="r" aria-label="(red bits)">--’-</span><span class="b" aria-label="(blue bits)">001</span>
        </samp>
      </td>
      <td>
        <samp>
          <u>1</u><ins>0</ins><mark>010</mark><span class="b" aria-label="(blue bits)">001</span>
          <u>1</u><ins>1</ins><mark>000</mark><span class="g" aria-label="(green bits)">100</span>
          <u>1</u><ins>1</ins><mark>001</mark><span class="r" aria-label="(red bits)">101</span>
        </samp>
      </td>
      <td>Hole in the middle, missing start, missing end.</td>
    </tr>
    <tr>
      <td>�</td>
      <td>
        <samp>
          <span class="g" aria-label="(green bits)">- --</span><span class="r" aria-label="(red bits)">10’1</span><span class="b" aria-label="(blue bits)">001</span>
        </samp>
      </td>
      <td>
        <samp>
          <u>1</u><ins>0</ins><mark>010</mark><span class="b" aria-label="(blue bits)">001</span>
          <u>1</u><ins>1</ins><mark>001</mark><span class="r" aria-label="(red bits)">101</span>
          <br>
          <u>1</u><ins>1</ins><mark>001</mark><span class="r" aria-label="(red bits)">101</span>
          <u>1</u><ins>1</ins><mark>000</mark><span class="g" aria-label="(green bits)">100</span>
        </samp>
      </td>
      <td>Missing rest, missing start.</td>
    </tr>
  </tbody>
</table>

This new encoding scheme seems to work as intended! But it’s
garbage. For every 3 bits of data we need 5 bits of meta
data. This is over 100% bloat! Our next goal is thus to
reduce the amount of meta data bits we currently use.

## We need *one* counter!

Let’s consider how likely and severe these faults are:

- Injecting or removing a single byte somewhere in the middle
  of text can easily happen. [Off-by-1 bugs][off1] are
  frighteningly common. We still want to be able to catch
  such errors.
- Swapping two bytes in the middle, however, as shown in the
  5<sup>th</sup> example of the terrible counter, is extremely
  unlikely.
- We sometimes cannot tell whether it’s *one* broken scalar
  value or *two*. But that’s fine as long as we know they’re
  definitely broken.
- Deleting or inserting a number of bytes in the middle such
  that the surrounding scalar values will be successfully
  recognised as one, or such that a single scalar value will
  be recognised as two, is so unlikely that chances are it
  was done on purpose.

We can pull an important conclusion from this: The only
counter that really matters is that of the first byte of a
sequence! With it alone we can already tell whether we have
too few or too many follow-up bytes. The old follow-up byte
counters only potentially help detecting byte swaps.

This means that the first byte of a sequence still only
encodes 3 payload bits. However, the remaining bytes of a
sequence can now encode 6 payload bits each. Not bad. Here’s
how this looks:

<table>
  <thead>
    <tr>
      <th>Dec.</th>
      <th>Binary (BE)</th>
      <th>Counter (LE, decent)</th>
      <th>Error</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>297</td>
      <td>
        <samp>
          <span class="r" aria-label="(red bits)">1 0010’1</span><span class="b" aria-label="(blue bits)">001</span>
        </samp>
      </td>
      <td>
        <samp>
          <u>1</u><ins>0</ins><mark>001</mark><span class="b" aria-label="(blue bits)">001</span>
          <u>1</u><ins>1</ins><span class="r" aria-label="(red bits)">100101</span>
        </samp>
      </td>
      <td>All good!</td>
    </tr>
    <tr>
      <td>�</td>
      <td>
        <samp>
          <span class="r" aria-label="(red bits)">- ----’-</span><span class="b" aria-label="(blue bits)">001</span>
        </samp>
      </td>
      <td>
        <samp>
          <u>1</u><ins>0</ins><mark>001</mark><span class="b" aria-label="(blue bits)">001</span>
        </samp>
      </td>
      <td>Missing rest.</td>
    </tr>
    <tr>
      <td>�</td>
      <td>�</td>
      <td>
        <samp>
          <u>1</u><ins>1</ins><span class="r" aria-label="(red bits)">100101</span>
        </samp>
      </td>
      <td>Missing start.</td>
    </tr>
  </tbody>
</table>

The payload bytes of this scheme do quite well. All they need
is a single bit to distinguish them from ASCII, and another
bit to distinguish them from the start of a sequence. We
cannot really do better than that. The starting byte still
uses up an unfortunate amount of meta data bits, however.

## Information density of the start byte

In our latest scheme, two bytes are capable of encoding…
just 9 bits of information. This is just barely enough to
encode all of ASCII, Latin‑1, Latin Extended‑A, and half of
Latin Extended‑B. We could bias <small>(read: offset)</small>
the encoded number by 128, given that encoding ASCII in two
bytes would be redundant. This would allow us to also cover
the rest of Latin Extended‑B. Not really an exciting amount
of coverage for two entire bytes. Needless to say, if any of
the designs so far had been the proposal for UTF‑8, we’d all
be using UTF‑16 now.

We want to optimise our encoding scheme for the case that
our sequence takes two bytes now, in order to maximise BMP
coverage. In theory, our starting byte only needs 3 bits of
meta data to serve its purpose.

- One bit tells it apart from ASCII.
- One bit marks it as the start of a sequence.
- One bit tells us whether this sequence has more than 2 bytes.

This leaves us with 5 payload bits in the starting byte if
our sequence is just 2 bytes long, giving us a total of 11
payload bits. Applying this same idea recursively gives us
4 payload bits in the starting byte of a 3‑bytes‑sequence,
or 16 bits in total, covering the entire BMP. An interesting
pattern that may allow us to clean up our current mess of
bit fields.

## An unary counter

Here’s the core idea:

- We start at the top-most bit of our starting byte, assuming
  our sequence is just 1 byte long.
- If it is a <samp>0</samp>, we have our number of bytes in
  the current sequence.
- If it is a <samp>1</samp>, our sequence is one byte longer
  than we thought. Continue checking the next bit.

In essence, we [count leading <samp>1</samp>s][cl1], which
is also known as an unary number system. Here’s what the
starting byte of any sequence may look like using this scheme:

<table>
  <caption>
    The unary counter bits are marked blue, the payload bits
    are represented by the letter <samp>x</samp> for we don’t
    care for any specific payload right now.
  </caption>
  <thead>
    <tr>
      <th>Starting Byte</th>
      <th># Follow-Up Bytes</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">0</span>xxx’xxxx
        </samp>
      </td>
      <td>0</td>
    </tr>
    <tr>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">10</span>xx’xxxx
        </samp>
      </td>
      <td>1</td>
    </tr>
    <tr>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">110</span>x’xxxx
        </samp>
      </td>
      <td>2</td>
    </tr>
    <tr>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">1110</span>’xxxx
        </samp>
      </td>
      <td>3</td>
    </tr>
    <tr>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">1111’0</span>xxx
        </samp>
      </td>
      <td>4</td>
    </tr>
    <tr>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">1111’10</span>xx
        </samp>
      </td>
      <td>5</td>
    </tr>
    <tr>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">1111’110</span>x
        </samp>
      </td>
      <td>6</td>
    </tr>
    <tr>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">1111’1110</span>
        </samp>
      </td>
      <td>7</td>
    </tr>
  </tbody>
</table>

This nicely covers our starting byte, but the follow-up bytes
need to be encoded efficiently as well. The bit pattern of a
follow-up byte must never be confusable for a start byte.
Remember that we found that 2 meta data bits are as good as
it gets for follow-up bytes. What if… we just removed the
second table row for the starting byte, then, using that
freed-up pattern for our follow-up bytes?

<table>
  <thead>
    <tr>
      <th>Starting Byte</th>
      <th># Follow-Up Bytes</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">0</span>xxx’xxxx
        </samp>
      </td>
      <td>0</td>
    </tr>
    <tr>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">110</span>x’xxxx
        </samp>
      </td>
      <td>1</td>
    </tr>
    <tr>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">1110</span>’xxxx
        </samp>
      </td>
      <td>2</td>
    </tr>
    <tr>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">1111’0</span>xxx
        </samp>
      </td>
      <td>3</td>
    </tr>
    <tr>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">1111’10</span>xx
        </samp>
      </td>
      <td>4</td>
    </tr>
    <tr>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">1111’110</span>x
        </samp>
      </td>
      <td>5</td>
    </tr>
    <tr>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">1111’1110</span>
        </samp>
      </td>
      <td>6</td>
    </tr>
  </tbody>
</table>

Well, the counter value is a bit wonky now, but for computers
it’s trivial to calculate:

```rust
let ones            = count_leading_ones(start_byte);
let follow_up_bytes = ones − (ones > 0);
```

And now this is the pattern of our follow-up bytes:

<table>
  <thead><tr><th>Follow-Up Byte</th></tr></thead>
  <tbody>
    <tr>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">10</span>xx’xxxx
        </samp>
      </td>
    </tr>
</table>

We can now detect and gracefully handle all the error cases
we explored and analysed in earlier sections, and do so while
squeezing as many payload bits as we can into every single
byte of a sequence.

<!-- TODO better ending, remark on UTF-16 byte order -->

## Removing nonsense

Let’s have a look at what a 4‑bytes sequence looks like in
our current scheme:

<table>
  <caption>
    Again the blue bits are our counter and tag bits, while
    the letter <samp>x</samp> marks a payload bit.
  </caption>
  <thead>
    <tr>
      <th>Starting Byte</th>
      <th>Follow-Up 1</th>
      <th>Follow-Up 2</th>
      <th>Follow-Up 3</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">1111’0</span>xxx
        </samp>
      </td>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">10</span>xx’xxxx
        </samp>
      </td>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">10</span>xx’xxxx
        </samp>
      </td>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">10</span>xx’xxxx
        </samp>
      </td>
    </tr>
</table>

If we count all the <samp>x</samp> bits together, we get…
exactly 21 bits, the magic lower limit of bits needed to
store every possible Unicode scalar value. So really we can
proclaim that all starting bytes that *would* ask for more
than 3 follow-up bytes are bogus, further shrinking our
starting byte table:

<table>
  <thead>
    <tr>
      <th>Starting Byte</th>
      <th># Follow-Up Bytes</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">0</span>xxx’xxxx
        </samp>
      </td>
      <td>0</td>
    </tr>
    <tr>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">110</span>x’xxxx
        </samp>
      </td>
      <td>1</td>
    </tr>
    <tr>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">1110</span>’xxxx
        </samp>
      </td>
      <td>2</td>
    </tr>
    <tr>
      <td>
        <samp>
          <span class="b" aria-label="(blue bits)">1111’0</span>xxx
        </samp>
      </td>
      <td>3</td>
    </tr>
  </tbody>
</table>

There are a lot more cases of redundancy we could cover. For
example, the sequence <samp>1100’000x 10xx’xxxx</samp> only
ever encodes the exact same scalar value as just
<samp>0xxx’xxxx</samp>, wasting a byte. And then there are
the UTF‑16 surrogates, which while being Unicode code points
are not Unicode scalar values. We can now choose how to
handle these edge cases:

- We could remove all these redundant encodings and the
  surrogate gap by biasing successive values. This is…
  complicated to do right.
- We could instead introduce specialised bounds checks
  at each step. Also annoying.
  - However, this allows us to split the handling of UTF-8
    text into two parts: A messy but correct validation pass,
    and a quick and dirty decoding pass that accepts surrogates
    and wasteful redundant encodings. We only rarely need the
    full validation.

Given our 21‑bit range and given that all these gaps and
redundancies don’t add up to being able to remove a whole
follow-up byte, we don’t really gain anything from the
complicated biasing technique, so let’s go for the bounds
checks instead.

## UTF-8 makes sense!

As you may have already guessed, this latest version of a
potential Unicode encoding scheme is… [exactly UTF-8][utf-8].
Now, how did we arrive here again?

- It all began with LEB128, the densest possible variable
  length encoding based on bytes.
- Given the context of where and how our byte-based text
  encoding scheme would be used, there are a number of
  fault tolerance requirements LEB128 just doesn’t meet.
- All that’s needed to satisfy these requirements is the
  addition of a counter and a start/rest bit.
- As it happens, a counter based on unary numbers serves
  all our needs while maximising information density.
- And that’s pretty much how you arrive at the design of
  UTF-8.

It’s nice, being able to appreciate how much damn good
engineering went into this encoding scheme.

----------------------------------------------------------------------

## Terms used
<dl>
  <dt>Unicode Scalar Value</dt>
  <dd>
    This is Unicode speak for what programmers would call
    <code>char</code> or <code>rune</code> or indeed
    <code>UnicodeScalarValue</code>, depending on which
    programming language they came from. Why this fancy and
    unfortunately quite unwieldy name? Well, what most
    people think of when talking about »characters« in text
    Unicode calls »extended grapheme cluster«. As it turns
    out, a single character can consist of one or
    arbitrarily many more scalar values. And what Unicode
    calls a »rune« is… well… actual letters of runic alphabets.
    So what is a scalar value now? A Unicode code point…
    <em>except</em> the few code points called »surrogates« that
    are needed to make UTF‑16 able to actually represent
    all Unicode scalar values.
  </dd>
  <dt>Little Endian</dt>
  <dt>LE</dt>
  <dd>
    These days all computer memory is usually addressable
    in units of bytes. However, we usually operate on pieces
    of data that are multiple bytes in size. For these, there are
    multiple ways they can be laid out in memory, though only
    two are relevant. Take the number <samp>0815<sub>16</sub></samp>,
    for example. We can split it up into the two bytes
    <samp>08<sub>16</sub></samp> and <samp>15<sub>16</sub></samp>.
    In Little Endian byte order, the bytes containing the lowest
    bits of a datum are put at the lower memory addresses. In
    Big Endian, the other relevant scheme, the higher bits of
    a datum are put at the lower addresses. Big Endian is the
    digit ordering you are familiar with from e.g. the English
    language.
    <br>
    <table>
      <caption>
        Memory layout of <samp>0815<sub>16</sub></samp> in
        different byte orders. <var>N</var> is the memory
        address from where we start writing our number.
      </caption>
      <thead>
        <tr>
            <th>Endian</th>
            <th>N+0</th>
            <th>N+1</th>
        </tr>
      </thead>
      <tbody>
        <tr>
            <th>Little</th>
            <td><samp>15<sub>16</sub></samp></td>
            <td><samp>08<sub>16</sub></samp></td>
        </tr>
        <tr>
            <th>Big</th>
            <td><samp>08<sub>16</sub></samp></td>
            <td><samp>15<sub>16</sub></samp></td>
        </tr>
      </tbody>
    </table>
  </dd>
  <dt>Big Endian</dt>
  <dt>BE</dt>
  <dd>The opposite byte order of LE.</dd>
  <dt>Basic Multilingual Plane</dt>
  <dt>BMP</dt>
  <dd>
    The <dfn>Basic Multilingual Plane</dfn> of Unicode,
    also known as plane 0, is a block of 65536 Unicode
    code points <small>(Yes, code points, the surrogates
    live here.)</small> that cover the vast majority of
    human text. Being able to represent all code points
    of the BMP up to at least the surrogates in as few
    bytes as possible is highly desirable.
  </dd>
  <dt>Basic Latin (aka. ASCII)</dt>
  <dt>Latin‑1 Supplement</dt>
  <dt>Latin Extended‑A</dt>
  <dt>Latin Extended‑B</dt>
  <dt>Phonetic Extensions</dt>
  <dt>Phonetic Extensions Supplement</dt>
  <dt>Latin Extended: The Empire Strikes Back</dt>
  <dt>Combining Diacritical Marks Supplement</dt>
  <dt>Latin Extended Additional</dt>
  <dt>Latin Extended‑C</dt>
  <dt>Latin Extended‑D</dt>
  <dt>Latin Extended‑E</dt>
  <dd>
    … and probably more i forgot or overlooked are sections
    of the Unicode BMP for <em>just</em> Latin script. Only
    one of these is made up.
  </dd>
  <dt>Unary Numbers</dt>
  <dd>
    In unary number systems there is only one digit. A number
    is represented by how often that one digit occurs. Say
    this digit is <samp>🐜</samp>. Then <samp>🐜🐜🐜🐜</samp>
    is the number 4. We can represent unary numbers inside a
    binary bit stream by picking one digit, say <samp>1</samp>,
    as our unary digit, and then counting all of them until
    we hit our first binary digit that does not match our
    unary one, say <samp>0</samp>. Or using ants again,
    <samp>🐜🐜🐜🏳️‍🌈🐜🐜🐜🐜</samp> represents the two
    separate numbers 3 and 4.
    <br>
    <small>(Side note: Dear font designers, fix the ant
    emoji, please! They often look <em>nothing</em> like an ant.
    Learn from Noto.)</small>
  </dd>
</dl>



[ascii]: https://en.wikipedia.org/wiki/ASCII
[unic]:  https://en.wikipedia.org/wiki/Unicode
[off1]:  https://en.wikipedia.org/wiki/Off-by-one_error
[cl1]:   https://en.wikipedia.org/wiki/Leading_zero
[utf-8]: https://en.wikipedia.org/wiki/UTF-8#Encoding
