+++
title       = "Evy’s Blog"
description = "A chaotic corner of the internet where i dump thoughts."
draft       = false

sort_by         = "date"
template        = "section.html"
page_template   = "page.html"
paginate_by     = 32
in_search_index = true
generate_feeds  = false

insert_anchor_links = "none"
+++

This blog is part of a small writing group called
[The Writing Gaggle][twg].
As the linked page states:<br>
<q>Our goal here is to motivate each other to write and
publish more</q>

[twg]: https://www.catmonad.xyz/writing_group/
