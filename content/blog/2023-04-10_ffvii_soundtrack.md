+++
title       = "What’s your soundtrack’s topology?"
description = "What if we built a graph of Final Fantasy VII’s soundtrack, where each piece is connected via common motifs or other stylistic commonalities? What would it look like? How many pieces would cluster together, how many would stand alone? Well, let’s find out."
draft       = true

[taxonomies]
categories = ["music"]
keywords   = ["music", "soundtrack", "ffvii", "graph_theory"]

[extra]
uuid = "056708a8-2cab-5fd0-8abd-1b2927228529"
+++


{% mxlMidi(asset='ffvii_soundtrack/ffvii_m_blood') %}
  Comparison of the opening melody of »2.13&nbsp;Trail&nbsp;of&nbsp;Blood«
  and the opening bass line of »2.12&nbsp;It’s&nbsp;Hard&nbsp;to&nbsp;Stand&nbsp;on&nbsp;Both&nbsp;Feet!«.
{% end %}
