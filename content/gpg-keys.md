+++
title           = "Evy’s GPG Keys"
description     = "My public GPG keys, so you can verify that stuff’s indeed from me!"
date            = 2021-09-16
template        = "page.html"
draft           = false
in_search_index = false

[taxonomies]

[extra]
no_feed = true
+++

Here’s a list of my public <abbr title="GNU Privacy Guard">GPG</abbr>
keys. In case you encounter any kind of digitally signed message that
allegedly came from me, one of these keys here is what you need to
cryptographically verify that claim. In case you do not know what all
this means, this page is likely not for you. You’d just waste your
time staring at strangely encoded data blobs.

<!-- more -->

None of these have anything to do with the
<abbr title="»Transport Layer Security«, aka. that thing that turns HTTP into HTTPS.">TLS</abbr>
certificates used by this website however.

## Keys for Git

I use these to sign all my commits, no matter which Git service.

- **ed25519** key
  <mark><samp>900FD17A972E24659917307BD588BC02EC2F2FAE</samp></mark>

  ```txt
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILP/SojqtgLP+dA4I4uoaWu+PACdHP90HSsceEJAh6P4 openpgp:0x5C3961C5
  ```

  ```txt
  -----BEGIN PGP PUBLIC KEY BLOCK-----

  mDMEXxM7VRYJKwYBBAHaRw8BAQdAV1ktI0VhyR5JxFFJBDEBFr3hfp/TwzAfveJ/
  p+hmXTa0MUV2cmV5IChUaGUgeDg2IElTQSBpcyBkdW1iKSA8ZXZyZXlAaGFja2lz
  aC5jb2Rlcz6IkAQTFggAOBYhBJAP0XqXLiRlmRcwe9WIvALsLy+uBQJfEztVAhsD
  BQsJCAcCBhUKCQgLAgQWAgMBAh4BAheAAAoJENWIvALsLy+uWCkA/2yC2EscFDJi
  MiIIFC5izIVV15Hgg7KyuoTcJVMzMLT2AQD3WhMGlhckp+uLzr66mYBMKX5qLF8T
  37C6ULderEZnCrg4BF8TO1USCisGAQQBl1UBBQEBB0C+0vctI89InulC/WojMUDN
  f6TnLb2JPTFgPwrsQAkjfwMBCAeIeAQYFggAIBYhBJAP0XqXLiRlmRcwe9WIvALs
  Ly+uBQJfEztVAhsMAAoJENWIvALsLy+uMB4BAIva1eA9sEbB8p1I1VTATL+isDAs
  FQWoU5KNwCygP10/AP9HsiMlNhSzfy2SdVoT8n+WKlbK3HqVTCgatE5Zfj3sAbgz
  BF80TRYWCSsGAQQB2kcPAQEHQLP/SojqtgLP+dA4I4uoaWu+PACdHP90HSsceEJA
  h6P4iHgEGBYIACAWIQSQD9F6ly4kZZkXMHvViLwC7C8vrgUCXzRNFgIbIAAKCRDV
  iLwC7C8vrgMmAP0Ye24bPNa4yLSGL/MOTWerHTX6oGmWh2qyeRoTf9saaQEA6Frv
  Ly4c5x9dImU03EvjKnVxGlDdj/KTPy07ZslAsgc=
  =4ofz
  -----END PGP PUBLIC KEY BLOCK-----
  ```
