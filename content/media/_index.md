+++
title       = "Evy’s Media Recommendations"
description = "A place of tastes and opinions for different media pieces i experienced. May it be of use to you, should you be searching for something new."
draft       = false

sort_by         = "date"
template        = "section.html"
page_template   = "page.html"
paginate_by     = 32
in_search_index = true
generate_feeds  = false

insert_anchor_links = "none"
+++
