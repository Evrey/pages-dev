+++
title       = "Index: Webcomics"
description = "A list of webcomics i’ve read and enjoyed, nicely tagged with quick visual content indicators so you can quickly find something that may interest you."
draft       = true

[taxonomies]
media_types = ["index", "webcomic"]
media_tags  = []

[extra]
uuid = "d77cfd1a-ed40-530c-bad0-a90027dc630a"
+++

You won’t find any star ratings here. These are not reviews, just general recommendations. A rating will not help you find a webcomic you’ll enjoy. On one hand, you may enjoy a »bad« comic quite a lot, which is often called a »guilty pleasure«. <small>(Do not feel guilty reading them.)</small> On the other hand, a comic may be an excellent work of art, but it may just not be your thing. Reviews are good for expectation management and for deepening one’s understanding of a work.

## Webcomics, Nicely Tagged

Ordered alphabetically, ignoring leading »the«s. The title links all lead you to the comic’s website.

***TODO***
- Harpy Gee
- Snarlbear
- Gunnerkrigg Court
- Phantomarine
- Cassiopeia Quinn
- The Hunters of Salamanstra
- Nevermore
- Hand Jumper
- Emmy the Robot
- Lady Knight
- Red Compass
- Finding Wonderland
- Nothing Special
- Garden Club Detective Squad
- AXED

<table>
  <thead>
    <tr>
      <th>Title</th>
      <th>Year</th>
      <th>{{mediaStatus()}}</th>
      <th>{{mediaDrama()}}</th>
      <th>{{mediaViolent()}}</th>
      <th>{{mediaSilly()}}</th>
      <th>{{mediaRomance()}}</th>
      <th>{{mediaLgbt()}}</th>
      <th>{{mediaLead()}}</th>
      <th>Description</th>
    </tr>
  </thead>

  <tbody>
    <tr>
    <tr>
      <td><a href="https://dresdencodak.com/2010/06/03/dark-science-01/" target="_blank" rel="noreferrer noopener">Dark Science</a></td>
      <td>2010</td>
      <td>{{mediaOngoing()}}</td>
      <td>{{mediaDrama()}}</td>
      <td></td>
      <td>{{mediaSilly()}}</td>
      <th>{{mediaRomance()}}</th>
      <td>{{mediaLgbt()}}</td>
      <td></td>
      <td>Kimiko Ross, a mad scientist, makes for Nephilopolis, the city of science, progress, and Arts Déco! I filed a form with the Department of Enjoyment for you to start reading the comic. Your enjoyment grant shall be delivered to your mailbox any moment, assuming said mailbox is properly registered with the Department of Physical Deliveries to Underspecified Addressees. In case your mailbox is yet unregistered, file a form with the Department of Bureaucracy to receive Registry Form A-38. Unfortunately the budget for the Department of Bureaucracy was assigned to the Department for Recursion, so currently everyone there’s on vacation.</td>
    </tr>
      <td><a href="https://www.daughterofthelilies.com/dotl/part-1-a-girl-with-no-face" target="_blank" rel="noreferrer noopener">Daughter of the Lilies</a></td>
      <td>2013</td>
      <td>{{mediaOngoing()}}</td>
      <td>{{mediaDrama()}}</td>
      <td></td>
      <td></td>
      <th>{{mediaRomance()}}</th>
      <td>{{mediaLgbt()}}</td>
      <td></td>
      <td>Thistle, a masked mage, so far only knew a life of hiding and running away. Something about her, unless kept secret, sparks terror and hatred. Yet, she still has to make a living, no matter how little Thistle came to think of herself. This time she joins your typical ragtag group of Tabletop RPG mercenary adventurers. Thistle’s time starts to tick. For how long can she enjoy their bonds of friendship until her secret’s found out? Certainly long enough to stumble upon the dark and ancient mysteries of the world around them.</td>
    </tr>
    <tr>
      <td><a href="" target="_blank" rel="noreferrer noopener">Godslave</a></td>
      <td>2014</td>
      <td>{{mediaOngoing()}}</td>
      <td>{{mediaDrama()}}</td>
      <td></td>
      <td></td>
      <th></th>
      <td></td>
      <td></td>
      <td>Edith found herself a new job, or rather the job found her, when she went for a visit to the local museum of Egyptian history. By accident, Edith breaks a magic jar in which an Egyptian god was sealed. Now she’s the god’s high priestess, whatever that encompasses. Among other things, running into the other gods, who secretly live among humans, and aren’t too happy about that jar being broken. An urban fantasy story without your usual vampires and werewolves. A much needed refresher for that genre.</td>
    </tr>
    <tr>
      <td><a href="https://www.gunnerkrigg.com/?p=1" target="_blank" rel="noreferrer noopener">Gunnerkrigg Court</a></td>
      <td>2005</td>
      <td>{{mediaOngoing()}}</td>
      <td>{{mediaDrama()}}</td>
      <td></td>
      <td>{{mediaSilly()}}</td>
      <th></th>
      <td></td>
      <td></td>
      <td>Antimony Carver</td>
    </tr>
    <tr>
      <td><a href="https://www.casualvillain.com/Unsounded/comic/ch01/ch01_01.html" target="_blank" rel="noreferrer noopener">Unsounded</a></td>
      <td>2010</td>
      <td>{{mediaOngoing()}}</td>
      <td>{{mediaDrama()}}</td>
      <td>{{mediaViolent()}}</td>
      <td></td>
      <th></th>
      <td></td>
      <td>{{mediaLeadMale()}}</td>
      <td>
        <p>A bratty thief girl is on a mission to collect a depth to her father, the »Lord of Thieves«. Escorting her is a powerful, highly educated and well-mannered mage… who’s also a zombie. What first appears to be a fun fantasy adventure quickly unravels into an epic story of war, culture, intrigue, magic, old gods, new gods, injustice, history, and a complex network of lies.</p>
        <p><em>The</em> most impressive webcomic i know, and by far my favourite. It’s a single person project by Ashley Cope, rich in immersive, at times even poëtic, writing. Her artworks are expressive, highly detailed, and skilled, even for mundane panels. You want to print out almost every page and hang it up as a poster. If you read it on the website, you’ll also enjoy a beautiful interplay between the comic pages themselves and the ever changing website background. For full immersion, put the website into fullscreen mode while reading. Trust me, it’s worth it while binge-reading. Artwork and writing aside, this webcomic also offers a fascinating story, a big cast of lovable and complex characters, highly detailed and immersive world building, and even a small amount of conlanging.</p>
        <p>Yes, i said no reviews. But i just can’t <em>not</em> praise this webcomic. If epic fantasy adventure is a genre you enjoy, then really give this one a try.</p>
      </td>
    </tr>
    <!--
    <tr>
      <td><a href="" target="_blank" rel="noreferrer noopener">Title</a></td>
      <td>Year</td>
      <td>{{mediaComplete()}}</td>
      <td>{{mediaDrama()}}</td>
      <td>{{mediaViolent()}}</td>
      <td>{{mediaSilly()}}</td>
      <th>{{mediaRomance()}}</th>
      <td>{{mediaLgbt()}}</td>
      <td>{{mediaLeadMale()}}</td>
      <td>Description</td>
    </tr>
    -->
  </tbody>

  <tfoot>
    <tr>
      <th>Title</th>
      <th>Year</th>
      <th>{{mediaStatus()}}</th>
      <th>{{mediaDrama()}}</th>
      <th>{{mediaViolent()}}</th>
      <th>{{mediaSilly()}}</th>
      <th>{{mediaRomance()}}</th>
      <th>{{mediaLgbt()}}</th>
      <th>{{mediaLead()}}</th>
      <th>Description</th>
    </tr>
  </tfoot>
</table>

## Explanation of Info Icons

To shorten the table’s width and make it more mobile-friendly, a number of symbols are used to annotate the webcomics. Here’s a quick listing of each symbol and what it means:

<dl>
  <dt>{{mediaDrama()}}</dt>
  <dd>The comic is dramatic, i.e. it focusses on emotional or social conflicts and their resolution.</dd>
  <dt>{{mediaSilly()}}</dt>
  <dd>The comic has a rather silly kind of humour. Expect weird things to happen.</dd>
  <dt>{{mediaViolent()}}</dt>
  <dd>The comic is rather violent. It doesn’t just feature physical conflict, it features excessive amounts or forms of it. Sometimes for humour, sometimes for shock value.</dd>
  <dt>{{mediaStatus()}}</dt>
  <dt>{{mediaOngoing()}}</dt>
  <dt>{{mediaComplete()}}</dt>
  <dt>{{mediaAxed()}}</dt>
  <dd>
    A comic’s completion status. {{mediaOngoing()}} is not yet finished and more chapters are in the works. Make sure to check this article’s date of publishing and last update. {{mediaComplete()}} is a comic where all chapters <small>(of the main story)</small> are released. {{mediaAxed()}} is a comic that’s on hiatus. While many webcomics successfully return from hiatus, many have just been cancelled without a word from their authors. Unfortunate, but making comics <em>is</em> an easy to underestimate amount of work, and you can burn out on making them rather quickly. On top of that, most webcomics are pasttime projects, and life changes. Please have some understanding for artists who axed their webcomics.
  </dd>
  <dt>{{mediaRomance()}}</dt>
  <dd>The story features romance. Not necessarily as the primary genre, but it plays an important part in the story.</dd>
  <dt>{{mediaLgbt()}}</dt>
  <dd>
    An openly <abbr title="Lesbian, Gay, Bisexual, Trans, and other people of the rainbow">LGBT+</abbr> comic. Some few examples <em>may</em> have LGBT+ representation that may come across as rather forceful.
  </dd>
  <dt>{{mediaLead()}}</dt>
  <dd>
    Most comics i read happen to have a woman or girl as the lead character. Where this is not the case, {{mediaLeadMale()}} is a comic with mostly male leading characters.
  </dd>
</dl>

Be aware that people have different ideas of how much drama is drama, or how much something else is something else. Take these icons as rough guideposts.

## Q&A

<ul>
</ul>

## The Graveyard

In memory of cancelled webcomics from the above list. Note that only rarely comic artists announce the cancellation of their projects. Usually, unfortunately, they stop working on them »for a while«, but then you realise that »for a while« has been years.

Please don’t be angry if a webcomic is cancelled or stuck indefinitely in hiatus. The people who feel worst about that are the artists themselves, and often their reason to put their work on hiarus is Real Life™.
