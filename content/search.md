+++
title           = "Search"
date            = 2023-02-01
template        = "search.html"
draft           = false
in_search_index = false

[taxonomies]

[extra]
no_feed = true
use_js  = true
+++
